<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-pc
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

define('INTERNAL', 1);
define('MENUITEM', 'content/pc');
define('SECTION_PLUGINTYPE', 'artefact');
define('SECTION_PLUGINNAME', 'pc');
define('SECTION_PAGE', 'view');
require(dirname(dirname(dirname(__FILE__))) . '/init.php');

safe_require('artefact', 'pc');

$pagetitle = hsc(get_string('viewcasenote', 'artefact.pc'));
define('TITLE', $pagetitle);

global $USER;

$id         = param_integer('id');
$returntype = param_variable('returntype', '');
$returnto   = param_variable('returnto', '');

$casenote = new ArtefactTypeCasenote($id);

$owner  = $casenote->get('owner');
$shared = $casenote->get('shared');

if (!($shared || $owner == $USER->id)) {
    throw new AccessDeniedException(get_string('youarenottheownerofthiscasenote', 'artefact.pc'));
}
$editable = ($owner == $USER->id) ? true : false;

$title            = $casenote->get('title');
$creator          = display_name($owner);
$date_created     = date('d/m/Y', $casenote->get('ctime'));
$date_encountered = date('d/m/Y', $casenote->get('etime'));
$conditionids     = $casenote->get('conditions');
$conditions = array();
foreach ($conditionids as $conditionid) {
    $container = new StdClass;
    $container->item = get_field('artefact_pc_item', 'item', 'id', $conditionid);
    $container->condition = get_string($container->item, 'artefact.pc');

    $conditions[] = $container;
}
$notes            = $casenote->get('description');
$attachments      = $casenote->get_casenote_attachments();

$sharednote = get_string('casenotesharedno', 'artefact.pc');
if ($shared) {
  $sharednote = get_string('casenotesharedyes', 'artefact.pc');
}

$connectjs = <<<JAVASCRIPT
var button = getFirstElementByTagAndClassName('a', 'sendmessage', 'main-column');
disconnectAll(button);
setNodeAttribute(button, 'title', "Preview");
connect(button, 'onclick', function (e) {
    e.stop();
    var sendmessage = $('sendmessageouter');
    showPreview('big', sendmessage);
});
JAVASCRIPT;

$js = <<<JAVASCRIPT
preview = DIV({'id':'viewpreview', 'class':'hidden'}, DIV({'id':'viewpreviewinner'}, DIV({'id':'viewpreviewcontent'})));

function showPreview(size, data) {
    $('viewpreviewcontent').innerHTML = data.innerHTML;
    var cancelbutton = getElementsByTagAndClassName('input', 'cancel', 'viewpreview');
    connect(cancelbutton[0], 'onclick', function (e) {e.stop(); fade(preview, {'duration':0.2});});

    var vdim = getViewportDimensions();
    var vpos = getViewportPosition();
    var offset = 16; // Left border & padding of preview container elements (@todo: use getStyle()?)
    if (size == 'small') {
        var width = 400;
        var xpos = (vdim.w - width - offset) / 2;
    } else {
        var width = vdim.w - 200;
        var xpos = vpos.x+100-offset;
    }
    // FIXME: IE doesn't like this...why?
    //setElementDimensions(preview, {'w':'100%'});
    setElementPosition(preview, {'x':xpos, 'y':vpos.y+200});
    showElement(preview);
}

addLoadEvent(function() {
    appendChildNodes(getFirstElementByTagAndClassName('body'), preview);
});
JAVASCRIPT;

$user = get_record('usr', 'id', $owner, 'deleted', 0);

if (!$user || (!can_send_message($USER->to_stdclass(), $owner) && !($owner == $USER->id))) {
	throw new AccessDeniedException(get_string('cantmessageuser', 'group'));
}

$user->introduction = get_field('artefact', 'title', 'artefacttype', 'introduction', 'owner', $owner);

$quote = get_string('commentoncasenote', 'artefact.pc', $title);
$form = pieform(array(
    'name' => 'sendmessage',
    'autofocus' => false,
    'jsform' => true,
    'elements' => array(
        'message' => array(
            'type'  => 'textarea',
            'title' => get_string('message'),
            'cols'  => 80,
            'rows'  => 10,
            'defaultvalue' => $quote,
        ),
        'submit' => array(
            'type' => 'submitcancel',
            'value' => array(get_string('sendmessage', 'group'), get_string('cancel')),
            'goto' => get_config('wwwroot') . 'artefact/pc/viewcasenote.php?id=' . $id,
        ),
        'owner' => array(
            'type' => 'hidden',
            'value' => $owner,
        ),
        'casenoteid' => array(
            'type' => 'hidden',
            'value' => $id,
        ),
    )
));

$smarty = smarty(array('tablerenderer'));
$smarty->assign_by_ref('PAGEHEADING', $pagetitle);
$smarty->assign_by_ref('connectjs', $connectjs);
$smarty->assign_by_ref('INLINEJAVASCRIPT', $js);
$smarty->assign('heading', null);
$smarty->assign('id', $id);
$smarty->assign('returntype', $returntype);
$smarty->assign('returnto', $returnto);
$smarty->assign('editable', $editable);
$smarty->assign('title', $title);
$smarty->assign('owner', $owner);
$smarty->assign('creator', $creator);
$smarty->assign('date_encountered', $date_encountered);
$smarty->assign('notes', $notes);
$smarty->assign_by_ref('attachments', $attachments);
$smarty->assign_by_ref('conditions', $conditions);
$smarty->assign('shared', $shared);
$smarty->assign('sharednote', $sharednote);
$smarty->assign('form', $form);
$smarty->assign('user', $user);

$smarty->display('artefact:pc:viewcasenote.tpl');

function sendmessage_submit(Pieform $form, $values) {
    global $USER, $SESSION;
    $user = get_record('usr', 'id', $values['owner']);
    send_user_message($user, $values['message'], null);
    $SESSION->add_ok_msg(get_string('messagesent', 'group'));
    redirect('/artefact/pc/viewcasenote.php?id=' . $values['casenoteid']);
}
