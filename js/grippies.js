/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-pc
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

/**
 * Retrieves the absolute position of an element on the screen
 * This function (C) 2006 Drupal
 */
function absolutePosition(el) {//{{{
    var sLeft = 0, sTop = 0;
    var isDiv = /^div$/i.test(el.tagName);
    if (isDiv && el.scrollLeft) {
        sLeft = el.scrollLeft;
    }
    if (isDiv && el.scrollTop) {
        sTop = el.scrollTop;
    }
    var r = { x: el.offsetLeft - sLeft, y: el.offsetTop - sTop };
    if (el.offsetParent) {
        var tmp = absolutePosition(el.offsetParent);
        r.x += tmp.x;
        r.y += tmp.y;
    }
    return r;
}//}}}

/**
 * This class based on Drupal's textArea class, which is (C) 2006 Drupal
 *
 * Provides a 'grippie' for resizing a textarea vertically.
 */
function PieformGrippie(element) {//{{{
    var self = this;

    this.element = element;
    this.dimensions = getElementDimensions(element);

    // Prepare wrapper
    this.wrapper = element.parentNode;

    // Add grippie and measure it
    this.grippie = TD({'class': 'cbgrippie'});
    this.grippie.dimensions = getElementDimensions(this.grippie);
    insertSiblingNodesAfter(this.wrapper, this.grippie);

    // Measure difference between desired and actual textarea dimensions to account for padding/borders
    this.widthOffset = getElementDimensions(this.wrapper).w - this.dimensions.w;

    // Make the grippie line up in various browsers
    if (window.opera) {
        setStyle(this.grippie, {'margin-right': '4px'});
    }
    if (document.all && !window.opera) {
        this.grippie.style.paddingLeft = '2px';
        setStyle(this.grippie, {
            'padding-left': '2px'
        });
    }
    this.element.style.MozBoxSizing = 'border-box';

    this.heightOffset = absolutePosition(this.grippie).y - absolutePosition(this.element).y - this.dimensions.h;

    this.widthOffset = absolutePosition(this.grippie).x - absolutePosition(this.element).x - this.dimensions.w;

    this.handleDrag = function (e) {//{{{
        // Get coordinates relative to text area
        var pos = absolutePosition(this.element);
        var y = e.mouse().client.y - pos.y;
        var x = e.mouse().client.x - pos.x;

        // Set new width
        var width = Math.max(32, x - this.dragOffset - this.widthOffset);
        setStyle(this.wrapper, {'width': width + this.grippie.dimensions.w + 1 + 'px'});
        setStyle(this.element, {'width': width + 'px'});

        // Avoid text selection
        e.stop();
    }//}}}

    this.endDrag = function (e) {//{{{
        disconnect(this.mouseMoveHandler);
        disconnect(this.mouseUpHandler);
        document.isDragging = false;
    }//}}}

    this.beginDrag = function(e) {//{{{
        if (document.isDragging) {
            return;
        }
        document.isDragging = true;

        self.mouseMoveHandler = connect(document, 'onmousemove', self, 'handleDrag');
        self.mouseUpHandler   = connect(document, 'onmouseup', self, 'endDrag');

        // Store drag offset from grippie top
        var pos = absolutePosition(this.grippie);
        //this.dragOffset = e.mouse().client.y - pos.y;
        this.dragOffset = e.mouse().client.x - pos.x;

        // Process
        this.handleDrag(e);
    }//}}}

    this.deleteGrippie = function() {
        removeElement(this.grippie);
    }

    connect(this.grippie, 'onmousedown', self, 'beginDrag');
}//}}}

