function ConditionSelector(idprefix, pathsep) {
    var self = this;
    this.id = idprefix;
    this.pathsep = pathsep;
    this.grippies = new Array();

    this.init = function () {
        var filtertype = $('filtertype');

        self.connectFilter(filtertype);
        self.rewriteBrowser();
        self.connectAddButton();
        self.connectSelected();
    }

    this.updateColumn = function (column, data) {
        var count = 0;
        forEach(getElementsByTagAndClassName('td', 'data', 'conditions-outer'), function (elem) {
            if (count > column) {
                removeElement(elem);
            };
            count++;
        });

        var elementId = 'i' + (column + 1);
        var element = $(elementId);
        if (!element) {
            var prevCellId = 't' + column;
            var prevCell = $(prevCellId);
            var cellId = 't' + (column + 1);
            var newCell = TD({'id': cellId, 'class': 'data'}, DIV({'id': elementId, 'class': 'conditions-inner'}));
            insertSiblingNodesAfter(prevCell, newCell);
            element = $(elementId);
            //newCell.scrollIntoView();
        };

        element.innerHTML = data.html;
        self.rewriteBrowser();
    }

    this.removeColumnsGreaterThan = function (column) {
        var count = 0;
        forEach(getElementsByTagAndClassName('td', 'data', 'conditions-outer'), function (elem) {
            if (count > column && count < self.grippies.length) {
                removeElement(elem);
            };
            count++;
        });
    }

    this.updateFilterType = function (data) {
        $('conditions-outer').innerHTML = data.html;
        self.rewriteBrowser();
    }

    this.updateSelected = function (element) {
        forEach(getElementsByTagAndClassName('li', 'selected', element.parentNode.parentNode), function(i) {
            removeElementClass(i, 'selected');
        });
        addElementClass(element.parentNode, 'selected');
    }

    this.updateParameters = function (params) {
        var parameters = $('parameters');
        parameters.value = params;
        parameters = $('parameters');
    }

    this.rewriteBrowser = function () {
        forEach(getElementsByTagAndClassName('li', 'itemlink', 'conditions'), function(i) {
            disconnectAll(i);
            connect(i, 'onclick', function (e) {
                e.stop();

                var href = getNodeAttribute(i.childNodes[0], 'href');
                var query = href.substring(href.indexOf('?')+1, href.length);
                var params = parseQueryString(query);
                var parameters = parseQueryString($('parameters').value);
                var column = parseInt(params.column);
                var children = parseInt(params.children);

                self.removeColumnsGreaterThan(column);
                self.updateSelected(i.childNodes[0]);

                path = params.path.split(self.pathsep);
                params.selected = path[path.length - 1];

                self.updateParameters(queryString(params));

                if (children) {
                    sendjsonrequest(config.wwwroot + 'artefact/pc/form/elements/conditionselector.json.php', params, 'POST', partial(self.updateColumn, column));
                    //i.scrollIntoView();
                }
            });
        });
        self.connectGrippies();
    }

    this.connectFilter = function (filtertype) {
        // Connect filter type select box
        disconnectAll(filtertype);
        connect(filtertype, 'onchange', function (e) {
            e.stop();
            var params = new Object();
            params.filtertype = filtertype.value;

            self.updateParameters(queryString(params));
            sendjsonrequest(config.wwwroot + 'artefact/pc/form/elements/conditionselector.json.php', params, 'POST', self.updateFilterType);
        });
    }

    this.addCondition = function (params) {
        var id = params.id;
        var name = params.name;
        var path = params.path.split(self.pathsep);
        params.value = path[path.length - 1];
        var value = params.value;
        var sc = $(self.id + '_selectlist');

        var tbody = $(self.id + '_selectlist_tbody');
        var rows = getElementsByTagAndClassName('tr', null, tbody);
        if (rows.length == 0) {
                removeElementClass(self.id + '_selectlist', 'hidden');
                addElementClass(self.id + '_empty_selectlist', 'hidden');
        }

        var exists = false;
        conditionId = self.id + '_selected[' + id + ']';

        for (i = 0; i < rows.length; i++) {
            if (rows[i].childNodes[1].childNodes[1].innerHTML == params.name) {
                exists = true;
                setNodeAttribute(rows[i].childNodes[1].childNodes[0], 'name', conditionId);
                setNodeAttribute(rows[i].childNodes[1].childNodes[0], 'value', id);
                setNodeAttribute(rows[i].childNodes[1].childNodes[1], 'id', conditionId);
                setNodeAttribute(rows[i].childNodes[1].childNodes[1], 'name', conditionId);
                setNodeAttribute(rows[i].childNodes[1].childNodes[1], 'value', id);
            }
        }

        if (!exists) {
            //var remove = BUTTON({'type': 'button', 'class': 'button small unselect', 'name': self.id + '_unselect[' + id + ']', 'value': get_string('remove')});
            var remove = INPUT({'type': 'button', 'class': 'button small unselect', 'name': self.id + '_unselect[' + id + ']', 'value': get_string('remove')});
            connect(remove, 'onclick', self.removeCondition);
            var condition = $(conditionId);
            if (!condition) {
                appendChildNodes(tbody,
                    TR({'class': 'r' + rows.length % 2},
                        TD(null, SPAN(null, name)),
                        TD(null, remove, INPUT({
                            'type': 'hidden',
                            'id': conditionId,
                            'name': conditionId,
                            'value': id
                        }))
                    )
                );
            }
        }
    }

    this.connectAddButton = function () {
        var addButton = $('addbutton');
        connect(addButton, 'onclick', function (e) {
            e.stop();

            var selectedElements = getElementsByTagAndClassName('li', 'selected', 'conditions-outer');
            var selected = selectedElements[(selectedElements.length-1)];
            if (typeof(selected) !== 'undefined' && selected != null) {
                var href = getNodeAttribute(selected.childNodes[0], 'href');
                var params = parseQueryString(href.substring(href.indexOf('?')+1, href.length));
                var column = parseInt(params.column);

                self.addCondition(params);
            }
        });
    }

    this.removeCondition = function (e) {
        e.stop();
        var id = this.name.replace(/.*_unselect\[(\d+)\]$/, '$1');
        removeElement(getFirstParentByTagAndClassName(this, 'tr'));
        var rows = getElementsByTagAndClassName('tr', null, self.id + '_selectlist');
        if (rows.length == 0) {
            addElementClass(self.id + '_selectlist', 'hidden');
            removeElementClass(self.id + '_empty_selectlist', 'hidden');
        }
        else {
            // Fix row classes
            for (var r = 0; r < rows.length; r++) {
                setNodeAttribute(rows[r], 'class', 'r' + r % 2);
            }
        }
    }

    this.connectSelected = function () {
        forEach(getElementsByTagAndClassName('button', 'unselect', self.id + '_selectlist'), function(i) {
            connect(i, 'onclick', self.removeCondition);
        });
    }

    this.connectGrippies = function () {
        forEach(getElementsByTagAndClassName('td', 'cbgrippie', 'conditions-outer'), function(i) {
            removeElement(i);
        });

        self.grippies = new Array();

        forEach(getElementsByTagAndClassName('div', 'conditions-inner', 'conditions-outer'), function(i) {
            self.grippies[self.grippies.length] = new PieformGrippie(i);
        });
    }

}

