function ConditionsBrowser(idprefix, pathsep) {
    var self = this;
    this.id = idprefix;
    this.pathsep = pathsep;
    this.grippies = new Array();

    this.init = function () {
        var filtertype = $('filtertype');
        var core = $('onlycore');
        var shared = $('includeshared');
        var resetfilter = $('resetfilter');

        self.connectFilter(core, shared, filtertype);
        self.connectCore(core, shared);
        self.connectShared(core, shared);
        self.connectReset(core, shared, resetfilter);
        self.rewriteBrowser();
        self.connectTableSort();
        casenotelist.postupdatecallback = self.connectViewLinks;
        casenotelist.postupdatecallback = self.connectDeleteLinks;
    }

    this.updateColumn = function (column, data) {
        var count = 0;
        forEach(getElementsByTagAndClassName('td', 'data', 'conditions-outer'), function (elem) {
            if (count > column) {
                removeElement(elem);
            };
            count++;
        });

        var elementId = 'i' + (column + 1);
        var element = $(elementId);
        if (!element) {
            var prevCellId = 't' + column;
            var prevCell = $(prevCellId);
            var cellId = 't' + (column + 1);
            var newCell = TD({'id': cellId, 'class': 'data'}, DIV({'id': elementId, 'class': 'conditions-inner'}));
            insertSiblingNodesAfter(prevCell, newCell);
            element = $(elementId);
        };

        element.innerHTML = data.html;
        self.rewriteBrowser();
    }

    this.removeColumnsGreaterThan = function (column) {
        var count = 0;
        forEach(getElementsByTagAndClassName('td', 'data', 'conditions-outer'), function (elem) {
            if (count > column && count < self.grippies.length) {
                removeElement(elem);
            };
            count++;
        });
    }

    this.updateFilterType = function (data) {
        $('conditions-outer').innerHTML = data.html;
        self.rewriteBrowser();
    }

    this.updateSelected = function (element) {
        forEach(getElementsByTagAndClassName('li', 'selected', element.parentNode.parentNode), function(i) {
            removeElementClass(i, 'selected');
        });
        addElementClass(element.parentNode, 'selected');
    }

    this.updateParameters = function (params) {
        var parameters = $('parameters');
        parameters.value = params;
        parameters = $('parameters');
    }

    this.rewriteBrowser = function () {
        forEach(getElementsByTagAndClassName('li', 'itemlink', 'conditions'), function(i) {
            disconnectAll(i);
            connect(i, 'onclick', function (e) {
                e.stop();

                var child = getFirstElementByTagAndClassName('a', 'itemlink', i);
                var href = getNodeAttribute(child, 'href');
                var query = href.substring(href.indexOf('?')+1, href.length);
                var params = parseQueryString(query);
                var parameters = parseQueryString($('parameters').value);
                var column = parseInt(params.column);
                var children = parseInt(params.children);

                self.removeColumnsGreaterThan(column);
                self.updateSelected(child);

                var core = $('onlycore');
                var shared = $('includeshared');
                params.shared = (shared.checked) ? 1 : 0;
                params.core = (core.checked) ? 1 : 0;
                params.offset = 0;
                params.sort = parameters.sort;
                params.order = parameters.order;

                path = params.path.split(self.pathsep);
                params.selected = path[path.length - 1];

                self.updateParameters(queryString(params));

                if (children) {
                    sendjsonrequest(config.wwwroot + 'artefact/pc/form/elements/conditionsbrowser.json.php', params, 'POST', partial(self.updateColumn, column));
                    params.action = 'filter';
                    casenotelist.doupdate(params);
                }
                else {
                    params.action = 'filter';
                    casenotelist.doupdate(params);
                }
            });
        });
        self.connectGrippies();
    }

    this.connectFilter = function (core, shared, filtertype) {
        // Connect filter type select box
        disconnectAll(filtertype);
        connect(filtertype, 'onchange', function (e) {
            e.stop();
            var params = new Object();
            var core = $('onlycore');
            var shared = $('includeshared');
            params.shared = (shared.checked) ? 1 : 0;
            params.core = (core.checked) ? 1 : 0;
            params.filtertype = filtertype.value;

            self.updateParameters(queryString(params));
            casenotelist.doupdate(params);
            sendjsonrequest(config.wwwroot + 'artefact/pc/form/elements/conditionsbrowser.json.php', params, 'POST', self.updateFilterType);
        });
    }

    this.connectCore = function (core, shared) {
        // Connect core checkbox
        disconnectAll(core);
        connect(core, 'onclick', function (e) {
            var params = parseQueryString($('parameters').value);
            params.column = -1;
            params.shared = (shared.checked) ? 1 : 0;
            params.core = (core.checked) ? 1 : 0;
            params.action = 'filter';

            sendjsonrequest(config.wwwroot + 'artefact/pc/form/elements/conditionsbrowser.json.php', params, 'POST', self.updateFilterType);
            casenotelist.doupdate(params);
        });
    }

    this.connectShared = function (core, shared) {
        // Connect shared checkbox
        disconnectAll(shared);
        connect(shared, 'onclick', function (e) {
            var params = parseQueryString($('parameters').value);
            params.column = -1;
            params.shared = (shared.checked) ? 1 : 0;
            params.core = (core.checked) ? 1 : 0;
            params.action = 'filter';

            sendjsonrequest(config.wwwroot + 'artefact/pc/form/elements/conditionsbrowser.json.php', params, 'POST', self.updateFilterType);
            casenotelist.doupdate(params);
        });
    }

    this.connectReset = function (core, shared, resetfilter) {
        // Connect reset filter button
        disconnectAll(resetfilter);
        connect(resetfilter, 'onclick', function (e) {
            e.stop();
            location.href = config.wwwroot + 'artefact/pc/';
        });
    }

    this.updateTableHeader = function (params) {
        // Update asc/desc indicator
        var oldSortColumn = getElementsByTagAndClassName('th', 'desc', 'casenotelist');
        if (oldSortColumn.length != 0) {
            removeElementClass(oldSortColumn[0], 'desc');
        }
        else {
            oldSortColumn = getElementsByTagAndClassName('th', 'asc', 'casenotelist');
            if (oldSortColumn.length != 0) {
                removeElementClass(oldSortColumn[0], 'asc');
            }
        }

        var newSortColumn = getElementsByTagAndClassName('th', params.sort, 'casenotelist');
        if (newSortColumn.length != 0) {
            if (params.order == 'A') {
                addElementClass(newSortColumn[0], 'asc');
            }
            else {
                addElementClass(newSortColumn[0], 'desc');
            }
        }
    }

    this.connectTableSort = function () {
        // Setup sorting of table columns
        forEach(getElementsByTagAndClassName('a', 'sort', 'casenotelist'), function(i) {
            disconnectAll(i);
            connect(i, 'onclick', function (e) {
                e.stop();

                var href = getNodeAttribute(i, 'href');
                var sortParams = parseQueryString(href.substring(href.indexOf('?')+1, href.length));
                var params = parseQueryString($('parameters').value);
                params.sort = sortParams.sort;
                params.order = sortParams.order;
                params.action = 'list';

                var core = $('onlycore');
                var shared = $('includeshared');
                params.shared = (shared.checked) ? 1 : 0;
                params.core = (core.checked) ? 1 : 0;

                self.updateParameters(queryString(params));
                // Update casenotelist
                casenotelist.doupdate(params);
                self.updateTableHeader(params);

                // Update params for future sorting
                var newSortColumn = getElementsByTagAndClassName('th', params.sort, 'casenotelist');
                var newHref = getNodeAttribute(newSortColumn[0].children[0], 'href');
                if (newHref[newHref.length-1] == 'A') {
                    newHref = newHref.replace(/order=A/,"order=D");
                }
                else {
                    newHref = newHref.replace(/order=D/,"order=A");
                }
                setNodeAttribute(newSortColumn[0].children[0], 'href', newHref);
            });
        });
    }

    this.connectViewLinks = function () {
        forEach(getElementsByTagAndClassName('a', 'viewcasenote', 'casenotelist'), function(i) {
            disconnectAll(i);
            connect(i, 'onclick', function (e) {

                var href = getNodeAttribute(i, 'href');
                var url = parseQueryString(href.substring(href.indexOf('?')+1, href.length));
                var params = parseQueryString($('parameters').value);
                if (typeof(params.path) !== 'undefined' && params.path != null) {
                    e.stop();
                    location.href = config.wwwroot + 'artefact/pc/viewcasenote.php?id=' + url.id + '&returntype=' + params.filtertype + '&returnto=' + params.path;
                }
            });
        });
    }

    this.connectDeleteLinks = function () {
        forEach(getElementsByTagAndClassName('div', 'editdelete', 'casenotelist'), function(i) {
            // first get the id of the element from the edit button
            var editbtn = getFirstElementByTagAndClassName('div', 'edit', i);
            var editlink = getFirstElementByTagAndClassName('a','btn', editbtn);
            var href = getNodeAttribute(editlink, 'href');
            var url = parseQueryString(href.substring(href.indexOf('?')+1, href.length));

            // filtertype and query string
            var params = parseQueryString($('parameters').value);

            // retrieve the delete link
            var deletebtn = getFirstElementByTagAndClassName('div', 'delete', i);
            var deletelink = getFirstElementByTagAndClassName('a','btn', deletebtn);
            disconnectAll(deletelink);

            connect(deletelink, 'onclick', function (e) {
                sendjsonrequest(
                    'delete.json.php',
                    {
                        'id': url.id
                    },
                    'POST',
                    function (data) {
                        if (typeof(params.path) !== 'undefined' && params.path != null) {
                            e.stop();
                            location.href = config.wwwroot + 'artefact/pc/index.php?filtertype=' + params.filtertype + '&q=' + params.path;
                        }
                    }
                );
            });
        });
    }

    this.connectGrippies = function () {
        forEach(getElementsByTagAndClassName('td', 'cbgrippie', 'conditions-outer'), function(i) {
            removeElement(i);
        });

        self.grippies = new Array();

        forEach(getElementsByTagAndClassName('div', 'conditions-inner', 'conditions-outer'), function(i) {
            self.grippies[self.grippies.length] = new PieformGrippie(i);
        });
    }

}
