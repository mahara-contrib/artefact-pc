<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-pc
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

define('INTERNAL', true);
define('MENUITEM', 'content/pc');
define('SECTION_PLUGINTYPE', 'artefact');
define('SECTION_PLUGINNAME', 'pc');
define('SECTION_PAGE', 'index');

require(dirname(dirname(dirname(__FILE__))) . '/init.php');
define('TITLE', hsc(get_string('mycasenotes', 'artefact.pc')));
safe_require('artefact', 'pc');

global $USER;
$wwwroot = get_config('wwwroot');

$filtertype = param_variable('filtertype', 'atoz');
$query = param_variable('q', '');
$magic = param_variable('magic', 0);
$order = param_alpha('order', 'D');
$sort = param_variable('sort', 'dateadded');

$enc_delete = json_encode(get_string('delete', 'artefact.pc'));
$enc_confirmdelete = json_encode(get_string('deletecasenote?', 'artefact.pc'));
$enc_editcasenote = json_encode(get_string('edit', 'artefact.pc'));
$nocasenotes = json_encode(get_string('nocasenotesorfiltered', 'artefact.pc'));
$enc_nofilteredcasenotes = json_encode(get_string('nofilteredcasenotes', 'artefact.pc'));
$params = (!empty($query)) ? "{'action': 'query', 'value': '$query', 'path': '$query', 'filtertype': '$filtertype'}" : '';

$form = pieform(array(
    'name'              => 'conditionsbrowser',
    'method'            => 'post',
    'action'            => '',
    'newiframeonsubmit' => true,
    'plugintype'        => 'artefact',
    'pluginname'        => 'pc',
    'configdirs'        => array(
        get_config('libroot') . 'form/',
        get_config('docroot') . 'artefact/pc/form/'
    ),
    'elements' => array(
        'browser' => array(
            'type' => 'fieldset',
            'legend' => get_string('filters', 'artefact.pc'),
            'collapsible' => true,
            'elements' => array(
                'conditionsbrowser' => array(
                    'type'       => 'conditionsbrowser',
                    'filtertype' => $filtertype,
                    'query'      => $query,
                    'magic'      => $magic,
                ),
            )
        )
    )
));


$js = <<<JAVASCRIPT

var casenotelist = new TableRenderer(
    'casenotelist',
    'index.json.php',
    [
        function(r) {
            var td = TD();
            td.innerHTML = r.type;
            return td;
        },
        function(r) {
            return TD(
                null,
                A({'class': 'viewcasenote', 'href':'{$wwwroot}artefact/pc/viewcasenote.php?id=' + r.cid}, r.title)
            );
        },
        function(r) {
            var td = TD();
            var conditions = '';
            for (var i = 0; i < r.conditions.length; i++) {
                if (i < (r.conditions.length - 1)) {
                    conditions += '<a href="{$wwwroot}artefact/pc/?magic=1&q=' + r.conditions[i].item + '">' + r.conditions[i].condition + '</a><br />';
                }
                else {
                    conditions += '<a href="{$wwwroot}artefact/pc/?magic=1&q=' + r.conditions[i].item + '">' + r.conditions[i].condition + '</a>';
                }
            }
            td.innerHTML = conditions;
            return td;
        },
        function(r) {
            var td = TD(null,
                DIV({'class': 'date'}, r.recorded, BR(), r.time)
            );
            return td;
        },
        function(r) {
            var sharedicon = (r.shared == 1) ? IMG({'src': '{$wwwroot}theme/raw/static/images/star.png', 'alt': 'Shared', 'class': 'cicon'}) : '';
            return TD({'class': 'shared'}, sharedicon);
        },
        function(r) {
            if (r.id == {$USER->get('id')} || r.shared != 1) {
                var editbutton = DIV({'class':'edit fl'}, A({'class': 'btn', 'href': '{$wwwroot}artefact/pc/editcasenote.php?id=' + r.cid}, {$enc_editcasenote}));

                var deleteButton = DIV({'class': 'delete fl'}, A({'class': 'btn', 'href': '', 'onclick': 'return false;'}, {$enc_delete}));
                connect(deleteButton, 'onclick', function (e) {
                    if (!confirm({$enc_confirmdelete})) {
                        return;
                    }
                    sendjsonrequest(
                        'delete.json.php',
                        {
                            'id': r.cid
                        },
                        'POST',
                        function (data) {
                            location.href = config.wwwroot + 'artefact/pc/';
                        }
                    );
                });
                return TD({'class': 'editdelete'}, DIV({'class': 'editdelete'}, editbutton, deleteButton));
            }
            else {
                return TD(null, A({'href': '{$wwwroot}user/view.php?id=' + r.id}, IMG({'src': r.profileiconurl, 'alt': ''}), ' ', r.displayname));
            }
        }
    ]
);

var nocasenotes = {$nocasenotes};
var nofilteredcasenotes = {$enc_nofilteredcasenotes};
casenotelist.emptycontent = nocasenotes;
casenotelist.updateOnLoad({$params});
JAVASCRIPT;

$smarty = smarty(array('tablerenderer'));
$smarty->assign('PAGEHEADING', hsc(get_string('mycasenotes', 'artefact.pc')));
$smarty->assign_by_ref('INLINEJAVASCRIPT', $js);
$smarty->assign_by_ref('casenotes', $casenotes);
$smarty->assign_by_ref('form', $form);
$smarty->assign('order', $order);
$smarty->assign('sort', $sort);
$smarty->display('artefact:pc:index.tpl');
