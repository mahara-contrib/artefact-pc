<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-pc
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'Problems and Conditions';

$string['add'] = 'Add';
$string['addcasenote'] = 'Add Case Note';
$string['attachedfiles'] = 'Attached files';
$string['attachments'] = 'Attachments';
$string['backtoresults'] = 'Back to results';
$string['by'] = 'by';
$string['casetitle'] = 'Case Title';
$string['casenote'] = 'Case Note';
$string['casenotes'] = 'Case Notes';
$string['casenotedeleted'] = 'Case Note deleted';
$string['casenotedoesnotexist'] = 'You are trying to access a case note that does not exist';
$string['casenotefilesdirdescription'] = 'Files uploaded as case note post attachments';
$string['casenotefilesdirname'] = 'casenotefiles';
$string['casenoteshared'] = 'Shared';
$string['casenotesharedyes'] = 'Yes';
$string['casenotesharedno'] = 'No';
$string['casenotesaved'] = 'Case note saved';
$string['clickhere'] = 'Click here';
$string['commentoncasenote'] = "Comment on '%s':\n";
$string['Condition'] = 'Conditions';
$string['conditions'] = 'Conditions';
$string['Conditions'] = 'Problems/Conditions covered';
$string['Dateadded'] = 'Date added';
$string['delete'] = 'Delete';
$string['deletecasenote?'] = 'Are you sure you want to delete this case note?';
$string['Date'] = 'Date';
$string['dateencountered'] = 'Date Encountered';
$string['edit'] = 'Edit';
$string['editcasenote'] = 'Edit Case Note';
$string['filters'] = 'Filters';
$string['filterlistby'] = 'Filter list by';
$string['includesharednotes'] = 'Include shared notes';
$string['invalidcolumn'] = 'Trying to fetch an invalid column (%s) for the Conditions Browser plugin';
$string['linkedto'] = 'Linked to';
$string['mycasenotes'] = 'My Case Notes';
$string['newcasenote'] = 'New Case Note';
$string['nocasenotes'] = 'You currently have no Case Notes';
$string['nocasenotesorfiltered'] = 'You either have no Case Notes, or none that match the current filter';
$string['noconditions'] = 'There are no conditions linked to this case note';
$string['nofiles'] = 'There are no files attached to this case note';
$string['nofilteredcasenotes'] = 'You have no Case Notes that match this filter';
$string['notes'] = 'Notes';
$string['onlydisplaycore'] = 'Only display core';
$string['problemsandconditions'] = 'Problems and Conditions';
$string['resetfilter'] = 'Reset Filter';
$string['savecasenote'] = 'Save Case Note';
$string['sendmessage'] = 'Send Message';
$string['sharecasenote'] = 'Share this note';
$string['toaddone'] = 'to add one';
$string['Title'] = 'Title';
$string['Type'] = 'Type';
$string['viewcasenote'] = 'View Case Note';
$string['youarenottheownerofthiscasenote'] = 'You are not the owner of this case note';
$string['youhavecasenotes'] ='You have %s Case Notes.';
$string['youhavenocasenotes'] = 'You have no Case Notes.';
$string['youhaveonecasenote'] ='You have one Case Note.';
$string['undefined'] = 'undefined';

$string['moreoptions'] = 'More options';
$string['copyfull'] = 'Others will get their own copy of your %s';
$string['copyreference'] = 'Others may display your %s in their View';
$string['copynocopy'] = 'Skip this block entirely when copying the View';

// Resync
$string['reloadconditions'] = 'Reload conditions';
$string['reloadconditionsdescription'] = 'Reload conditions from your xml file %s.  The new file will be merged with your existing data.';
$string['resyncsuccessful'] = 'Successfully reloaded conditions.xml';
$string['resyncfailed'] = 'Failed to load conditions.xml';
$string['resyncerror'] = "Please consult Mahara's error log for more information.";
$string['deleteconditions'] = 'Delete conditions';
$string['deleteconditionsdescription'] = 'Existing case notes will not be deleted, but all associated conditions will be removed from them.  You will need to reload a new XML file, and manually re-tag all case notes. You probably don\'t want to do this on a production site.';
$string['restorefromtags'] = 'Restore case note conditions';
$string['restorefromtagsdescription'] = 'If your case notes have lost associated conditions but still have tags (for example because you deleted all conditions and reloaded them from the xml file), this will read each case note tag, and reassociate any matching condition with the case note.';

// Categories
$string['atoz'] = 'A-Z';
$string['specialty'] = 'Specialty';

// Items
$string['a'] = 'A';
$string['b'] = 'B';
$string['c'] = 'C';
$string['d'] = 'D';
$string['e'] = 'E';
$string['f'] = 'F';
$string['g'] = 'G';
$string['h'] = 'H';
$string['i'] = 'I';
$string['j'] = 'J';
$string['k'] = 'K';
$string['l'] = 'L';
$string['m'] = 'M';
$string['n'] = 'N';
$string['o'] = 'O';
$string['p'] = 'P';
$string['q'] = 'Q';
$string['r'] = 'R';
$string['s'] = 'S';
$string['t'] = 'T';
$string['u'] = 'U';
$string['v'] = 'V';
$string['w'] = 'W';
$string['x'] = 'X';
$string['y'] = 'Y';
$string['z'] = 'Z';
$string['abdominalpain'] = 'Abdominal Pain';
$string['abdominalpainchild'] = 'Abdominal Pain (Child of)';
$string['abdominalpainchildchild'] = 'Abdominal Pain (Child of Child of)';
$string['cancer'] = 'Cancer';
$string['cataracts'] = 'Cataracts';
$string['child of mastisis'] = 'Child of Mastisis';
$string['encology'] = 'Encology';
$string['opthalmology'] = 'Opthalmology';
$string['mastisis'] = 'Mastisis';
$string['schizophrenia'] = 'Schizophrenia';
$string['pediatrics'] = 'Pediatrics';
$string['psychiatry'] = 'Psychiatry';
$string['surgery'] = 'Surgery';

// Translate any tag in the db as itself if not defined above
if (@$items = get_column('artefact_pc_item', 'item')) {
    foreach ($items as $i) {
        if (!isset($string[$i])) {
            $string[$i] = $i;
        }
    }
}
