<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-pc
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

// Some string that won't appear in an item and can appear in a param
define('ARTEFACTPC_PATHSEP', ';');

class PluginArtefactPc extends PluginArtefact {
    public static function get_artefact_types() {
        return array(
            'casenote',
        );
    }

    public static function get_block_types() {
        return array();
    }

    public static function get_plugin_name() {
        return 'pc';
    }

    public static function menu_items() {
        return array(
            array(
                'path'   => 'content/pc',
                'url'    => 'artefact/pc/',
                'title'  => get_string('casenotes', 'artefact.pc'),
                'weight' => 80,
            ),
        );
    }

    public static function block_advanced_options_element($configdata, $artefacttype) {
        $strartefacttype = get_string($artefacttype, 'artefact.pc');
        return array(
            'type' => 'fieldset',
            'name' => 'advanced',
            'collapsible' => true,
            'collapsed' => false,
            'legend' => get_string('moreoptions', 'artefact.pc'),
            'elements' => array(
                'copytype' => array(
                    'type' => 'select',
                    'title' => get_string('blockcopypermission', 'view'),
                    'description' => get_string('blockcopypermissiondesc', 'view'),
                    'defaultvalue' => isset($configdata['copytype']) ? $configdata['copytype'] : 'nocopy',
                    'options' => array(
                        'nocopy' => get_string('copynocopy', 'artefact.pc'),
                        'reference' => get_string('copyreference', 'artefact.pc', $strartefacttype),
                        'full' => get_string('copyfull', 'artefact.pc', $strartefacttype),
                    ),
                ),
            ),
        );
    }

    public static function postinst($prevversion) {
        if ($prevversion == 0) {
            self::resync_conditions();
        }
    }

    /**
     * Resyncs the db with the casenotes.xml file.
     *
     * This can be called on install and in the plugin configuration form
     */
    function resync_conditions() {
        ini_set('max_execution_time', 120);
        db_begin();
        $sync_info = array();
        log_info('Beginning resync of conditions list');

        $filename = get_config('docroot') . 'artefact/pc/conditions.xml';
        if (!$xml = simplexml_load_file($filename)) {
            // TODO: bail out in a much nicer way...
            throw new UserException("FATAL: XML file is not well formed! Please consult Mahara's error log for more information");
        }

        $version = $xml->xpath('version');
        if ((string)$version[0] !== '2') {
            throw new UserException("FATAL: expected problems and conditions version 2 xml format.  See sample_conditions.xml file.");
        }

        $_categories = get_column_sql("SELECT category FROM {artefact_pc_category}", array());
        $_xcategories = array_map('ltrim', $xml->xpath('categories/category/name'));

        $_items = get_column_sql("SELECT item FROM {artefact_pc_item}", array());
        $_xitems = array_map('ltrim', $xml->xpath('items/item/name'));

        $_add = new StdClass;
        $_remove = new StdClass;
        $_compare = new StdClass;

        $_add->categories = array_diff($_xcategories, $_categories);
        $_add->items = array_diff($_xitems, $_items);
        $_remove->categories = array_diff($_categories, $_xcategories);
        $_remove->items = array_diff($_items, $_xitems);
        $_compare->items = array_intersect($_xitems, $_items);

        /*** ADD CATEGORIES ***/
        foreach ($_add->categories as $name) {
            $record = new StdClass;
            $record->category = $name;
            $sync_info[] = 'Add category: ' . $name;
            insert_record('artefact_pc_category', $record);
        }

        /*** ADD ITEMS ***/
        // Insert all new items
        foreach ($_add->items as $name) {
            $record = new StdClass;
            $record->item = $name;

            if (strpos($name, ARTEFACTPC_PATHSEP) !== false) {
                throw new UserException('FATAL: Item ' . $name . ' contains path separator "' . ARTEFACTPC_PATHSEP . '". '
                                        . "Either change this item's name or change the separator string.");
            }

            $nodes = $xml->xpath("//item[name=\"$name\"]");

            $item_parents = $xml->xpath("//item[name=\"$name\"]/parents/parent");

            $needscategory = false;
            if (!empty($item_parents)) {
                foreach ($item_parents as $item_parent) {
                    if ((string)$item_parent->name == 'ROOT') {
                        $needscategory = true;
                        break;
                    }
                }
            }
            $needscategory |= $name == 'ROOT';

            $item_cats = $xml->xpath("//item[name=\"$name\"]/categories/category");

            if (empty($item_cats) && $needscategory) {
                throw new UserException('FATAL: Item ' . $name . ' is missing a category');
            }

            if (!empty($item_cats)) {
                foreach ($item_cats as $item_cat) {
                    $record->category = (string) $item_cat;
                    insert_record('artefact_pc_item', $record);
                }
            }
            else {
                insert_record('artefact_pc_item', $record);
            }
        }

        /*** ADD ITEM MAPPING ***/
        // Once new items have been inserted, go and add their mappings
        foreach ($_add->items as $name) {
            $item_parents = $xml->xpath("//item[name=\"$name\"]/parents/parent");
            $item_cats = $xml->xpath("//item[name=\"$name\"]/categories/category");

            if (!empty($item_parents)) {
                foreach ($item_parents as $item_parent) {
                    $parentname = (string) $item_parent->name;
                    if (!empty($item_cats)) {
                        foreach ($item_cats as $item_cat) {
                            $catname = (string) $item_cat;
                            $record = new StdClass;
                            $parent = get_field('artefact_pc_item', 'id', 'item', $parentname, 'category', $catname);
                            $child  = get_field('artefact_pc_item', 'id', 'item', $name, 'category', $catname);
                            if (empty($parent)) {
                                throw new UserException("FATAL: Cannot fetch id for $parentname, $catname from the database");
                            }
                            if (empty($child)) {
                                throw new UserException("FATAL: Cannot fetch id for $name, $catname from the database");
                            }
                            $record->parentid    = $parent;
                            $record->childid     = $child;
                            if ($item_parent->core) {
                                $record->childiscore = true;
                            }
                            insert_record('artefact_pc_itemmap', $record);
                        }
                    }
                    else {
                        $record = new StdClass;
                        $parent = get_field('artefact_pc_item', 'id', 'item', $parentname);
                        $child  = get_field('artefact_pc_item', 'id', 'item', $name);
                        if (empty($parent)) {
                            throw new UserException("FATAL: Cannot fetch id for $parentname from the database");
                        }
                        if (empty($child)) {
                            throw new UserException("FATAL: Cannot fetch id for $name from the database");
                        }
                        $record->parentid = $parent;
                        $record->childid = $child;
                        if ($item_parent->core) {
                            $record->childiscore = true;
                        }
                        insert_record('artefact_pc_itemmap', $record);
                    }
                }
            }
        }

        // Get all the item data for convenience, indexed by item & category
        $dbitems = get_records_assoc(
            'artefact_pc_item', '', '', '',
            "CASE WHEN item = 'ROOT' THEN category || ';ROOT' ELSE item END AS itemkey, item, id, category"
        );

        /*** COMPARE ITEMS AND ITEM MAPPING ***/
        foreach ($_compare->items as $name) {
            $nodes = $xml->xpath("//item[name=\"$name\"]");

            $item_parents = $xml->xpath("//item[name=\"$name\"]/parents/parent");

            $needscategory = false;
            if (!empty($item_parents)) {
                foreach ($item_parents as $item_parent) {
                    if ((string)$item_parent->name == 'ROOT') {
                        $needscategory = true;
                        break;
                    }
                }
            }

            $needscategory |= $name == 'ROOT';

            $item_cats = $xml->xpath("//item[name=\"$name\"]/categories/category");

            if (empty($item_cats) && $needscategory) {
                throw new UserException('FATAL: Item ' . $name . ' is missing a category');
            }

            $dbcats = get_column('artefact_pc_item', 'category', 'item', $name);

            if (count($dbcats) == 1 && is_null($dbcats[0])) {
                $dbcats = array();
            }

            if (array_diff($dbcats, $item_cats)) {
                throw new UserException('Item ' . $name . ': cannot update categories yet');
            }

            if ($name == 'ROOT') {
                continue; // No parents
            }

            $category = !empty($item_cats) ? (string) $item_cats[0] : null;

            $dbparents = get_records_sql_assoc("
                SELECT
                    p.item AS name, im.*
                FROM {artefact_pc_item} c
                    JOIN {artefact_pc_itemmap} im ON c.id = im.childid
                    JOIN {artefact_pc_item} p ON p.id = im.parentid
                WHERE
                    c.item = ?",
                array($name)
            );

            $new = array();
            foreach ($item_parents as $p) {
                $parentname  = (string) $p->name;
                $parentindex = $parentname;
                $childindex  = $name;
                if ($category) {
                    $parentindex = $category . ';' . $parentindex;
                }
                $record = (object) array(
                    'parentid'    => $dbitems[$parentindex]->id,
                    'childid'     => $dbitems[$childindex]->id,
                    'childiscore' => (int) !empty($p->core),
                );

                $new[$dbitems[$parentindex]->id] = true;
                if (isset($dbparents[$parentname])) {
                    if ($dbparents[$parentname]->childiscore == $record->childiscore) {
                        continue;
                    }
                    set_field(
                        'artefact_pc_itemmap',
                        'childiscore', $record->childiscore,
                        'childid', $record->childid,
                        'parentid', $record->parentid
                    );
                    continue;
                }
                insert_record('artefact_pc_itemmap', $record);
            }

            if ($dbparents) {
                foreach ($dbparents as $p) {
                    if (!isset($new[$p->parentid])) {
                        delete_records(
                            'artefact_pc_itemmap',
                            'childid', $p->childid,
                            'parentid', $p->parentid
                        );
                    }
                }
            }
        }

        // Remove records from artefact_pc_item2casenote, artefact_pc_itemmap, and artefact_pc_item
        // NOTE: Only items that are being completely removed will be removed here, all other items
        // to be removed will be removed in the comparison section
        foreach ($_remove->items as $name) {
            $itemids = get_column('artefact_pc_item', 'id', 'item', $name);
            foreach ($itemids as $id) {
                delete_records('artefact_pc_item2casenote', 'itemid', $id);
                delete_records('artefact_pc_itemmap', 'parentid', $id);
                delete_records('artefact_pc_itemmap', 'childid', $id);
                delete_records('artefact_pc_item', 'id', $id);
            }
        }

        // Remove records from artefact_pc_category
        foreach ($_remove->categories as $name) {
            $sync_info[] = 'Delete category: ' . $name . ' from artefact_pc_category';
            delete_records('artefact_pc_category', 'category', $name);
        }

        log_info('Finishing resync of conditions list');

        db_commit();
    }
}

class ArtefactTypeCasenote extends ArtefactType {

    protected $etime;
    protected $shared;
    protected $conditions;
    protected $conditionsfull;
    const pagination = 10;

    /**
     * We override the constructor to fetch the extra data.
     *
     * @param integer
     * @param object
     */
    public function __construct($id = 0, $data = null) {
        parent::__construct($id, $data);

        if ($this->id) {
           ($this->conditionsfull = get_records_sql_assoc("
                SELECT i.id, i.category, i.item FROM {artefact_pc_item} i
                WHERE i.id IN (
                    SELECT itemid
                    FROM {artefact_pc_item2casenote}
                    WHERE casenoteid = ?
                )
                ORDER BY i.item",
                array(
                    $this->id
                )
            )) || ($this->conditionsfull = array());

            $this->conditions = array();
            foreach($this->conditionsfull as $condition) {
                $this->conditions[] = $condition->id;
            }

            if ($properties = get_record('artefact_pc_casenote', 'id', $this->id)) {
                foreach((array)$properties as $field => $value) {
                    if (property_exists($this, $field)) {
                        if (in_array($field, array('etime'))) {
                            $value = strtotime($value);
                        }
                        $this->{$field} = $value;
                    }
                }
            }
        }
    }

    /**
     * This function updates or inserts the artefact.  This involves putting
     * some data in the artefact table (handled by parent::commit()), and then
     * some data in the artefact_pc_casenote and artefact_pc_item2casenote tables.
     */
    public function commit() {
        // Just forget the whole thing when we're clean.
        if (empty($this->dirty)) {
            return;
        }

        // We need to keep track of newness before and after.
        $new = empty($this->id);

        // Commit to the artefact table.
        parent::commit();

        // Reset dirtyness for the time being.
        $this->dirty = true;

        $id         = $this->get('id');
        $conditions = $this->get('conditions');

        $properties = (object)array(
            'id'     => $id,
            'etime'  => db_format_timestamp($this->get('etime')),
            'shared' => $this->get('shared'),
        );

        // Look up item text for the selected conditions - required for tags
        if ($conditions) {
            $items = get_records_sql_assoc(
                "SELECT id, item AS tag FROM {artefact_pc_item} WHERE id IN ( " . join(',', $conditions) . " )",
                array()
            );
        }
        else {
            $items = array();
        }

        if ($new) {
            insert_record('artefact_pc_casenote', $properties);
        }
        else {
            update_record('artefact_pc_casenote', $properties);
            delete_records('artefact_pc_item2casenote', 'casenoteid', $id); // Deleted here to be re-inserted below
        }

        // Insert conditions and matching tags
        if (!empty($conditions)) {
            foreach ($conditions as $condition) {
                $data = (object)array(
                    'casenoteid' => $id,
                    'itemid'     => $condition,
                );
                insert_record('artefact_pc_item2casenote', $data);

                // Insert tag for this condition - existing tags will already have been purged in /artefact/lib.php
                if (!empty($items[$condition]->tag)) {
                    if (!in_array($items[$condition]->tag,$this->tags)) {  // Exclude duplicate user inputted tags
                        insert_record(
                            'artefact_tag',
                            (object) array(
                                'artefact' => $id,
                                'tag'      => $items[$condition]->tag,
                            )
                        );
                    }
                }
            }
        }

        $this->dirty = false;
    }

    /**
     * This function extends ArtefactType::delete() by also deleting any
     * extra data in the casenote table.
     */
    public function delete() {
        if (empty($this->id)) {
            return;
        }

        $this->detach(); // Detach all file attachments

        delete_records('artefact_pc_item2casenote', 'casenoteid', $this->id);
        delete_records('artefact_pc_casenote', 'id', $this->id);

        parent::delete();
    }

    /**
     * Checks that the person viewing this casenote is the owner.
     */
    public function check_permission() {
        global $USER;
        if ($USER->get('id') != $this->owner) {
            throw new AccessDeniedException(get_string('youarenottheownerofthiscasenote', 'artefact.pc'));
        }
    }

    public function render_self($options) {
        $smarty = smarty_core();
        if (empty($options['hidetitle'])) {
            if (isset($options['viewid'])) {
                $smarty->assign('artefacttitle', '<a href="' . get_config('wwwroot') . 'view/artefact.php?artefact='
                     . $this->get('id') . '&amp;view=' . $options['viewid']
                     . '">' . hsc($this->get('title')) . '</a>');
            }
            else {
                $smarty->assign('artefacttitle', hsc($this->get('title')));
            }
        }

        $smarty->assign('artefactdescription', $this->get('description'));

        $smarty->assign('artefact', $this);

        $attachments = $this->get_attachments();
        if ($attachments) {
            $this->add_to_render_path($options);
            require_once(get_config('docroot') . 'artefact/lib.php');
            foreach ($attachments as &$attachment) {
                $f = artefact_instance_from_id($attachment->id);
                $attachment->size = $f->describe_size();
                $attachment->iconpath = $f->get_icon(array('id' => $attachment->id, 'viewid' => isset($options['viewid']) ? $options['viewid'] : 0));
                $attachment->viewpath = get_config('wwwroot') . 'view/artefact.php?artefact=' . $attachment->id . '&view=' . (isset($options['viewid']) ? $options['viewid'] : 0);
                $attachment->downloadpath = get_config('wwwroot') . 'artefact/file/download.php?file=' . $attachment->id;
                if (isset($options['viewid'])) {
                    $attachment->downloadpath .= '&view=' . $options['viewid'];
                }
            }
            $smarty->assign('attachments', $attachments);
        }

        $shared = $this->get('shared');
        $smarty->assign('shared', $shared);

        $date_encountered = date('d/m/Y', $this->get('etime'));
        $smarty->assign('date_encountered', $date_encountered);

        $conditionids     = $this->get('conditions');
        $conditions = array();
        foreach ($conditionids as $conditionid) {
            $container = new StdClass;
            $container->item = get_field('artefact_pc_item', 'item', 'id', $conditionid);
            $container->condition = get_string($container->item, 'artefact.pc');

            $conditions[] = $container;
        }
        $smarty->assign('conditions', $conditions);


        return array('html' => $smarty->fetch('artefact:pc:render/casenote_renderfull.tpl'),
                     'javascript' => '');
    }

    public function can_have_attachments() {
        return true;
    }

    public static function is_singular() {
        return false;
    }

    /**
     * Returns links to case note artefacts to the search page.
     */
    public static function get_links($id) {
        $wwwroot = get_config('wwwroot');

        return array(
            '_default' => $wwwroot . 'artefact/pc/viewcasenote.php?id=' . $id,
        );
    }

    public static function casenote_files_folder_id($create = true) {
        global $USER;
        $name = get_string('casenotefilesdirname', 'artefact.pc');
        $description = get_string('casenotefilesdirdescription', 'artefact.pc');
        safe_require('artefact', 'file');
        return ArtefactTypeFolder::get_folder_id($name, $description, null, $create, $USER->get('id'));
    }

    /**
     * This function returns a list of the current user's casenotes
     *
     * @param User
     * @param integer
     * @param integer
     */
    public static function get_casenotes(User $user, $limit = self::pagination, $offset = 0, $filter = array()) {
        $type = (isset($filter['type'])) ? $filter['type'] : 'atoz';
        $core = (!empty($filter['core'])) ? true : false;
        $shared = (!empty($filter['shared'])) ? true : false;
        $value = (!empty($filter['value'])) ? true : false;

        if ($value) {
            $itemgraph = self::get_itemgraph($type, $core, $shared, $filter['value']);

            $selectionid = get_field('artefact_pc_item', 'id', 'item', $filter['value']);
            $childids = self::get_children_of_item($selectionid);
            $selectionid_all_categories = get_column('artefact_pc_item', 'id', 'item', $filter['value']);
            $childids = array_merge($childids, $selectionid_all_categories);
        }

        // Build the query
        $query = array();
        $sqlvalues = array();

        $query[] = "SELECT a.id AS cid, " . db_format_tsfield('a.ctime', 'recorded') . ", a.artefacttype AS type, a.title AS title, " . db_format_tsfield('c.etime', 'encountered') . ", c.shared, u.id, u.username, u.preferredname, u.firstname, u.lastname, u.admin";
        $query[] = "FROM {artefact} a";
        $query[] = "  JOIN {usr} u ON (a.owner = u.id)";
        $query[] = "  JOIN {artefact_pc_casenote} c ON (a.id = c.id)";
        $query[] = "WHERE a.artefacttype = 'casenote'";
        if ($shared) {
            $query[] = "  AND (a.owner = ? OR c.shared = ?)";
            $sqlvalues[] = $user->get('id');
            $sqlvalues[] = $filter['shared'];
        }
        else {
            $query[] = "  AND a.owner = ?";
            $sqlvalues[] = $user->get('id');
        }
        // Find casenotes with core conditions attached
        if ($value || $core) {
            $query[] = "  AND a.id IN (";
            $query[] = "    SELECT cn.id";
            $query[] = "    FROM {artefact_pc_casenote} cn";
            $query[] = "    WHERE id IN (";
            $query[] = "      SELECT casenoteid FROM {artefact_pc_item2casenote} WHERE itemid IN (";
            if ($core) {
                $query[] = "        SELECT DISTINCT childid FROM {artefact_pc_itemmap} WHERE childiscore = 1";
                if ($value) {
                    $query[] = "  AND childid IN (" . implode(',', array_map('intval', $childids)) . ')';
                }
            }
            else {
                $query[] = implode(',', array_map('intval', $childids));
            }
            $query[] = "    )";
            $query[] = "    ORDER BY cn.id)";
            $query[] = "  )";
        }
        $order = ($filter['order'] == 'A') ? " ASC" : " DESC";
        $query[] = "ORDER BY";
        switch ($filter['sort']) {
          case 'dateadded':
            $query[count($query) - 1] .= " a.ctime" . $order . ", a.title ASC";
            break;
          case 'shared':
            $query[count($query) - 1] .= " c.shared" . $order . ", a.title ASC";
            break;
          case 'title':
            $query[count($query) - 1] .= " a.title" . $order;
            break;
          case 'type':
            $query[count($query) - 1] .= " a.artefacttype" . $order . ", a.title ASC";
            break;
        }
        $query[count($query) - 1] .= " LIMIT ? OFFSET ?";
        $sqlvalues[] = $limit;
        $sqlvalues[] = $offset;

        $sql = implode("\n", $query);;

        // View the filter, query, and values to be inserted
        //log_debug($filter);
        //log_debug($sql);
        //log_debug($sqlvalues);

        ($result = get_records_sql_assoc($sql, $sqlvalues)) || ($result = array());

        foreach ($result as $key => $casenote) {
            // Set displayname
            $result[$key]->displayname = display_name($result[$key]);
            // Set profileiconurl
            $result[$key]->profileiconurl = get_config('wwwroot') . 'thumb.php?type=profileicon&id=' . $result[$key]->id . '&size=20x20';

            // Get conditions
            if ($result[$key]->type == 'casenote') {
                $result[$key]->type = get_string('casenote', 'artefact.pc');
            }

            $ctime = $result[$key]->recorded;
            $result[$key]->recorded = date('d-m-Y', $ctime);
            $result[$key]->time = '' . date('H:i', $ctime);

            // TODO: query inside foreach loop bad bad BAD
            $items_sql = "
                SELECT i.item FROM {artefact_pc_item} i
                WHERE i.id IN (SELECT itemid FROM {artefact_pc_item2casenote} WHERE casenoteid = ?)
                ORDER BY i.item";
            $items_sqlvalues = array($result[$key]->cid);

            ($items = get_records_sql_array(
                $items_sql, $items_sqlvalues
            )) || ($items = array());

            $result[$key]->conditions = array();
            foreach ($items as $item) {
                $container = new StdClass;

                $name = get_string($item->item, 'artefact.pc');
                $container->condition = $name;
                $container->item = $item->item;

                $result[$key]->conditions[] = $container;
            }
        }

        // Rework the query to get a count
        $query[0] = "SELECT COUNT(*)";
        // Get rid of the ORDER BY
        array_pop($query);
        $sql = implode("\n", $query);
        // Get rid of the now unused limit & offset sqlvalues
        array_pop($sqlvalues);
        array_pop($sqlvalues);

        // View the query and values to be inserted
        //log_debug($sql);
        //log_debug($sqlvalues);

        $count = (int)get_field_sql($sql, $sqlvalues);
        return array($count, array_values($result));
    }

    public function can_view_artefact_files() {
        if ($this->shared) {
            return true;
        }
        global $USER;
        return $USER->can_view_artefact($a);
    }

    /**
     * This function returns an array of attachments.
     */
    public function get_casenote_attachments() {
        $attachments = $this->get_attachments();
        if ($attachments) {
            $this->add_to_render_path($options);
            require_once(get_config('docroot') . 'artefact/lib.php');
            foreach ($attachments as &$attachment) {
                $f = artefact_instance_from_id($attachment->id);
                $attachment->size = $f->describe_size();
                $attachment->downloadpath = get_config('wwwroot') . 'artefact/pc/download.php?file=' . $attachment->id . '&casenote=' . $this->id;
                $attachment->iconpath = $this->get_icon(array('id' => $attachment->id, 'casenote' => $this->id, 'filedownload' => true));
            }
            return $attachments;
        }
        return array();
    }

    public static function get_icon($options=null) {
        if (isset($options['filedownload'])) {
            $url = get_config('wwwroot') . 'artefact/pc/download.php?';
            $url .= 'file=' . $options['id'];
            $url .= '&casenote=' . $options['casenote'];

            if (isset($options['size'])) {
                $url .= '&size=' . $options['size'];
            }
            else {
                $url .= '&maxheight=20&maxwidth=20';
            }

            return $url;
        }
    }

    public static function has_config() {
        return true;
    }

    public static function get_config_options() {
        $elements = array(
            'delete' => array(
                'type'         => 'checkbox',
                'class'        => 'error',
                'title'        => get_string('deleteconditions', 'artefact.pc'),
                'description'  => get_string('deleteconditionsdescription', 'artefact.pc'),
                'defaultvalue' => false,
            ),
            'resync' => array(
                'type'         => 'checkbox',
                'title'        => get_string('reloadconditions', 'artefact.pc'),
                'description'  => get_string('reloadconditionsdescription', 'artefact.pc', get_config('docroot') . 'artefact/pc/conditions.xml'),
                'defaultvalue' => false,
            ),
            'restorefromtags' => array(
                'type'         => 'checkbox',
                'title'        => get_string('restorefromtags', 'artefact.pc'),
                'description'  => get_string('restorefromtagsdescription', 'artefact.pc'),
                'defaultvalue' => false,
            ),
        );

        return array(
            'elements' => $elements,
        );
    }

    public static function validate_config_options($form, $values) {
        if ($values['delete']) {
            delete_records('artefact_pc_item2casenote');
            delete_records('artefact_pc_itemmap');
            delete_records('artefact_pc_item');
            delete_records('artefact_pc_category');
        }
        if ($values['resync']) {
            try {
                PluginArtefactPc::resync_conditions();
                // $SESSION->add_ok_msg(get_string('resyncsuccessful', 'artefact.pc'));
            }
            catch (UserException $e) {
                $form->set_error('resync', get_string('resyncfailed', 'artefact.pc') . ': ' . $e->getMessage());
            }
            catch (SystemException $e) {
                $form->set_error(
                    'resync',
                    get_string('unrecoverableerror', 'error') . ' ' . get_string('resyncerror', 'artefact.pc'));
            }
        }
        if ($values['restorefromtags']) {
            $ctags = get_records_sql_array("
                SELECT a.id, t.tag
                FROM {artefact} a JOIN {artefact_tag} t ON a.id = t.artefact
                WHERE a.artefacttype = 'casenote'",
                array()
            );
            $citems = get_records_sql_assoc("
                SELECT itemid || ';' || casenoteid, itemid, casenoteid
                FROM {artefact_pc_item2casenote}",
                array()
            );
            $items = get_records_assoc('artefact_pc_item', '', '', '', 'item, id, category');
            if ($ctags) {
                foreach ($ctags as $t) {
                    if (isset($items[$t->tag])) {
                        $itemid = $items[$t->tag]->id;
                        if (!isset($citems[$itemid . ';' . $t->id])) {
                            insert_record(
                                'artefact_pc_item2casenote',
                                (object) array(
                                    'casenoteid' => $t->id,
                                    'itemid'     => $itemid,
                                )
                            );
                        }
                    }
                }
            }
        }
    }

    public static function save_config_options($values) {
    }

    private static $itemgraph = null;

    /**
     * Returns a graph of the items using their parent/children relationships.
     * Any item can have one or more parents and any number of children
     * (though the root node has no parent of course). This is a directed
     * acyclic graph with one root node.
     *
     * This graph also includes the casenotes tagged with the given item.
     */
    public static function get_itemgraph($type, $core=0, $shared=0, $value='') {
        if (!is_null(self::$itemgraph)) {
            return self::$itemgraph;
        }
        self::$itemgraph = array();

        global $USER;
        $userid = $USER->get('id');

        $rootitemid = get_field('artefact_pc_item', 'id', 'category', $type, 'item', 'ROOT');


        // Shunt items into the graph, so the graph looks like:
        // array(
        //     itemid => array(
        //         items => array(
        //             item-with-childid-that-equals-itemid,
        //             item-with-childid-that-equals-itemid,
        //             ...
        //         ),
        //     ),
        //     itemid => ....
        // )
        //
        $wherefield = $wherevalue = '';
        if ($core) {
            $wherefield = 'childiscore';
            $wherevalue = 1;
        }
        $itemmap = get_records_array('artefact_pc_itemmap', $wherefield, $wherevalue);
        // TODO: not sure if next line is needed
        $itemmap[] = (object)array('childid' => $rootitemid, 'parentid' => null, 'count' => 0);
        foreach ($itemmap as $item) {
            $cid = $item->childid;
            if (isset(self::$itemgraph[$cid])) {
                self::$itemgraph[$cid]->items[] = $item;
            }
            else {
                self::$itemgraph[$cid] = (object)array(
                    'items' => array($item),
                );
            }
        }
        unset($itemmap);

        // Prebuild a list of child items for each item
        foreach (self::$itemgraph as $childid => $node) {
            if ($childid == $rootitemid) continue;
            foreach ($node->items as $item) {
                self::$itemgraph[$item->parentid]->children[] = $childid;
            }
        }

        //
        // Now add the casenotes that are tagged with a given item to the tree
        //

        // Get all case notes we should be seeing into a list of casenoteid => array(item, item), ...
        $sharedsql = '';
        if ($shared) {
            $sharedsql = 'UNION
                SELECT itemid, casenoteid
                FROM {artefact_pc_item2casenote} i2c
                JOIN {artefact_pc_casenote} cn ON (i2c.casenoteid = cn.id AND cn.shared = 1)';
        }
        if ($records = get_records_sql_array('SELECT itemid, casenoteid
            FROM {artefact_pc_item2casenote} i2c
            JOIN {artefact_pc_casenote} cn ON (i2c.casenoteid = cn.id)
            JOIN {artefact} a ON (cn.id = a.id AND a.owner = ?)'
            . $sharedsql, array($userid))) {
            foreach ($records as $note) {
                if (isset(self::$itemgraph[$note->itemid])) {
                    $casenotes[$note->casenoteid][] = $note->itemid;
                }
            }
        }

        // Now assign case notes to the tree
        if (isset($casenotes)) {
            self::assign_casenotes_to_itemgraph($rootitemid, $casenotes);
        }

        return self::$itemgraph;

    }

    /**
     * NOTE: this function is called recursively, so make sure you don't add
     * anything that might be slow - aka, move everything you can OUTSIDE this
     * function
     */
    private static function assign_casenotes_to_itemgraph($itemid, &$casenotes) {
        self::$itemgraph[$itemid]->casenotes = array();

        foreach ($casenotes as $note => $casenoteitems) {
            if (in_array($itemid, $casenoteitems)) {
                self::$itemgraph[$itemid]->casenotes[] = $note;
            }
        }

        $childcasenotes = array();
        if (isset(self::$itemgraph[$itemid]->children)) {
            foreach (self::$itemgraph[$itemid]->children as $childid) {
                $childcasenotes = array_merge($childcasenotes, self::assign_casenotes_to_itemgraph($childid, $casenotes));
            }
        }

        self::$itemgraph[$itemid]->casenotes = array_unique(array_merge($childcasenotes, self::$itemgraph[$itemid]->casenotes));
        return self::$itemgraph[$itemid]->casenotes;
    }

    private static function get_children_of_item($itemid) {
        if (!isset(self::$itemgraph[$itemid]->children)) {
            return array();
        }

        $children = array();
        foreach (self::$itemgraph[$itemid]->children as $child) {
            $children = array_merge($children, self::get_children_of_item($child));
        }

        return array_merge($children, self::$itemgraph[$itemid]->children);
    }

    /**
     * Looks through the casenote text for links to download artefacts, and
     * returns the IDs of those artefacts.
     */
    public function get_referenced_artefacts_from_casenote() {
        return artefact_get_references_in_html($this->get('description'));
    }

}
