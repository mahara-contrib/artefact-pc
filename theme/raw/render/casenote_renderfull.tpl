<div class="viewcasenote">
    <div class="indented">
        <div class="notes">{$artefactdescription|clean_html|safe}</div>

        <div class="encountered">{str tag="dateencountered" section="artefact.pc"}: {$date_encountered}</div>

        {if $attachments}
        <table class="casenote-attachments">
          <tbody>
            <tr><th colspan="2">{str tag=attachedfiles section=artefact.pc}</th></tr>
        {if $attachments}
        {foreach from=$attachments item=item}
            <tr class="r{cycle values='1,0'}">
              <td style="width: 22px;"><img src="{$item->iconpath}" alt=""></td>
              <td><a href="{if $item->viewpath}{$item->viewpath}{else}{$item->downloadpath}{/if}">{$item->title}</a> ({$item->size}) - <strong><a href="{$item->downloadpath}">{str tag=Download section=artefact.file}</a></strong>
              <br><strong>{$item->description}</strong></td>
            </tr>
        {/foreach}
        {else}
            <tr class="r{cycle values='1,0'}">
              <td colspan="2">{str tag=nofiles section=artefact.pc}</a></strong>
            </tr>
        {/if}
          </tbody>
        </table>
        {/if}
    </div>

    <div class="conditions-section">
        <h4 class="conditions-title">{str tag="Conditions" section="artefact.pc"}</h4>
        <div class="conditions indented">
            {foreach from=$conditions key=key item=condition name=conditions}
            <a href="{$WWWROOT}artefact/pc/?magic=1&q={$condition->item}">{$condition->condition}</a>{if $smarty.foreach.conditions.last}{else}, {/if}
            {foreachelse}
            {str tag="noconditions" section="artefact.pc"}
            {/foreach}
        </div>
    </div>

    {if $shared}
    <div class="shared">
        <span class="shared-title">{str tag="casenoteshared" section="artefact.pc"}:</span><img src="{$WWWROOT}theme/raw/static/images/star.png" alt="Shared" class="cicon" />
    </div>
    {/if}
</div>

