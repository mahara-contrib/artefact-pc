<div id="conditions">
    <script type="text/javascript">
        {$initjs|safe}
    </script>
    <div id="conditions-header">
        <h3>{str section="artefact.pc" tag="problemsandconditions"}</h3>
    </div>
    <div class="clearer"></div>
    {if $categories}
    <div id="category-select">
        <label for="filtertype">{str section="artefact.pc" tag="filterlistby"}:</label>
        <select id="filtertype">
            {foreach from=$categories->items key=itemkey item=category}
            <option{if $itemkey == $filtertype} selected="selected"{/if} value="{$itemkey}">{$category}</option>
            {/foreach}
        </select>
    </div>
    <div class="clearer"></div>
    {/if}
    <div id="conditions-outer">
        {include file="artefact:pc:form/conditionselector.tpl"}
    </div>
    <div id="addbutton-container">
        &nbsp;<br />
        &nbsp;
        <div class="fr" id="addbutton">
            <a class="btn" href="#" onclick="return false;">{str section="artefact.pc" tag="add"}</a>
        </div>
    </div>
    <input type="hidden" id="parameters" value="filtertype={$filtertype}{if $path}&path={$path}{/if}" />
</div>
<div id="linked-conditions">
    <h5>{str section="artefact.pc" tag="linkedto"}:</h5>
    {include file="artefact:pc:form/conditionselector_selectedlist.tpl"}
</div>
