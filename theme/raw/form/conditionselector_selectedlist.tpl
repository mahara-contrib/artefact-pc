    <p id="{$prefix}_empty_selectlist"{if $selectedlist} class="hidden"{/if}>{str section="artefact.pc" tag="noconditions"}</p>
    <table id="{$prefix}_selectlist"{if !$selectedlist} class="hidden"{/if}>
     <!--
     <thead>
      <tr>
       <th>{str section="artefact.pc" tag="Condition"}</th>
       <th></th>
      </tr>
     </thead>
     -->
     <tbody id="{$prefix}_selectlist_tbody">
      {foreach from=$selectedlist item=condition}
      <tr class="r{cycle values='0,1'}">
        <td>
            <span>{$condition->name}</span>
        </td>
        <td>
           <button type="button" class="button small unselect" name="{$prefix}_unselect[{$condition->id}]" value="{$condition->id}">{str tag=remove}</button>
           <input type="hidden" id="{$prefix}_selected[{$condition->id}]" name="{$prefix}_selected[{$condition->id}]" value="{$condition->id}">
        </td>
      </tr>
      {/foreach}
     </tbody>
    </table>

