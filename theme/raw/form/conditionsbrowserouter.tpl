<div id="conditions">
    <script type="text/javascript">
        {$initjs|safe}
    </script>
    <div id="conditions-header">
        <h3>{str section="artefact.pc" tag="problemsandconditions"}</h3>
    </div>
    <span class="fr">
        <a class="btn" href="#" onclick="return false;" id="resetfilter">{str section="artefact.pc" tag="resetfilter"}</a>
    </span>
    <div class="clearer"></div>
    {if $categories}
    <div id="category-select">
        <label for="filtertype">{str section="artefact.pc" tag="filterlistby"}:</label>
        <select id="filtertype">
            {foreach from=$categories->items key=itemkey item=category}
            <option{if $itemkey == $filtertype} selected="selected"{/if} value="{$itemkey}">{$category}</option>
            {/foreach}
        </select>
    </div>
    <div class="clearer"></div>
    {/if}
    <div id="conditions-outer">
        {include file="artefact:pc:form/conditionsbrowser.tpl"}
    </div>
    <input type="hidden" id="parameters" value="filtertype={$filtertype}{if $path}&path={$path}{/if}" />
    <div id="checkboxes">
        <label for="onlycore">{str section="artefact.pc" tag="onlydisplaycore"}:</label>
        <input type="checkbox" id="onlycore" value="no" />
        <label for="includeshared">{str section="artefact.pc" tag="includesharednotes"}:</label>
        <input type="checkbox" id="includeshared" value="no" />
    </div>
</div>
