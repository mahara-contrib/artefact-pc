    {if $data}
    {counter start=0 assign=count}
        <table><tr>
        {foreach from=$data key=key item=columndata}
            <td id="t{$count}" class="data">
                <div id="i{$count}" class="conditions-inner">
                    {include file="artefact:pc:form/conditionselectorcolumn.tpl"}
                </div>
            </td>
        {counter}
        {/foreach}
        </tr></table>
    {/if}
