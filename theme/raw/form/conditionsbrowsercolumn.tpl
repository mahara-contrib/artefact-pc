                {if $columndata}
                <ul>
                    {foreach from=$columndata->items key=key item=row}
                    <li class="itemlink{if $row->itemkey == $columndata->selected} selected{/if}{if $row->children > 0} children{/if}"><a href="index.php?filtertype={$filtertype}&column={$count}{if $row->path}&path={$row->path}{/if}&children={if $row->children > 0}1{else}0{/if}{if $core}&core={$core}{/if}" class="itemlink">{$row->name} - ({if $row->count}{$row->count}{else}0{/if})</a></li>
                    {/foreach}
                </ul>
                {/if}
