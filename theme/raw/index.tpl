{*

  This template displays a list of the user's casenotes.  The list is populated
  using javascript.

 *}

{include file="header.tpl"}
            <div id="mycasenotes">
                <div class="rbuttons">
                  <a class="btn" href="{$WWWROOT}artefact/pc/addcasenote.php">{str section="artefact.pc" tag="addcasenote"}</a>
                </div>
                {if $form}
                <div>{$form|safe}</div>
                {/if}
                <table id="casenotelist" class="hidden tablerenderer">
                    <colgroup>
                        <col class="type" />
                        <col class="title" />
                        <col class="conditions" />
                        <col class="dateadded" />
                        <col class="shared" />
                        <col class="editdelete" />
                    </colgroup>
                    <thead><tr>
                        <th class="type">
                            <a class="sort" href="index.php?sort=type&order=A">{str section="artefact.pc" tag=Type}</a>
                        </th>
                        <th class="title">
                            <a class="sort" href="index.php?sort=title&order=A">{str section="artefact.pc" tag=Title}</a>
                        </th>
                        <th class="conditions">
                            {str section="artefact.pc" tag=Conditions}
                        </th>
                        <th class="dateadded desc">
                            <a class="sort" href="index.php?sort=dateadded&order=A">{str section="artefact.pc" tag=Dateadded}</a>
                        </th>
                        <th class="shared">
                            <a class="sort" href="index.php?sort=shared&order=A">{str section="artefact.pc" tag=casenoteshared}</a>
                        </th>
                        <th class="editdelete"></th>
                    </tr></thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
{include file="footer.tpl"}
