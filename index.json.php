<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-pc
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

define('INTERNAL', 1);
define('JSON', 1);

require(dirname(dirname(dirname(__FILE__))) . '/init.php');
safe_require('artefact', 'pc');

$action = param_variable('action', 'list');
$id = param_variable('id', 0);
$limit = param_integer('limit', ArtefactTypeCasenote::pagination);
$offset = param_integer('offset', 0);

$core = param_integer('core', 0);
$filtertype = param_variable('filtertype', 'atoz');
$order = param_alpha('order', 'D');
$shared = param_integer('shared', 0);
$sort = param_variable('sort', 'dateadded');
$path = param_variable('path', '');
$column = param_variable('column', '');

$values = explode(ARTEFACTPC_PATHSEP, $path);
$value = (!empty($values)) ? $values[count($values) - 1] : '';

json_headers();

if ($action != 'delete') {
    $filter = array(
        'core'      => $core,
        'type'      => $filtertype,
        'order'     => $order,
        'shared'    => $shared,
        'sort'      => $sort,
        'value'     => $value,
    );

    list($count, $data) = ArtefactTypeCasenote::get_casenotes($USER, $limit, $offset, $filter);

    echo json_encode(array(
        'count'  => $count,
        'data'   => $data,
        'limit'  => $limit,
        'offset' => $offset,
    ));
}
else if ($action == 'delete') {
    $casenote = artefact_instance_from_id($id);
    if ($casenote instanceof ArtefactTypeCasenote) {
        $casenote->check_permission();
        $casenote->delete();
        json_reply(false, get_string('casenotedeleted', 'artefact.pc'));
    }

    throw new ArtefactNotFoundException(get_string('casenotedoesnotexist', 'artefact.pc'));
}
