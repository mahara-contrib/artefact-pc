<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-pc
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

function xmldb_artefact_pc_upgrade($oldversion=0) {
    // There was no database prior to this version.
    if ($oldversion < 2009040600) {
        $location = get_config('docroot') . 'artefact/pc/db/';
        install_from_xmldb_file($location . 'install.xml');
    }

    if ($oldversion < 2009041500) {
        $table = new XMLDBTable('artefact_pc_shared');
        $table->addFieldInfo('id', XMLDB_TYPE_INTEGER, 10, null, XMLDB_NOTNULL);
        $table->addFieldInfo('shared', XMLDB_TYPE_INTEGER,  1, null, XMLDB_NOTNULL, null, null, null, 0);
        $table->addKeyInfo('sharedfk', XMLDB_KEY_FOREIGN, array('id'), 'artefact', array('id'));
        create_table($table);
    }

    if ($oldversion < 2009041600) {
        if (is_postgres()) {
            execute_sql("
            ALTER TABLE {artefact_pc_casenote} DROP CONSTRAINT {artepccase_id_pk};
            ALTER TABLE {artefact_pc_casenote} ADD FOREIGN KEY (id) REFERENCES {artefact}(id);
            ");
        }
        else if (is_mysql()) {
            execute_sql("ALTER TABLE {artefact_pc_casenote} DROP PRIMARY KEY");
            execute_sql("ALTER TABLE {artefact_pc_casenote} ADD FOREIGN KEY(id) REFERENCES {artefact}(id)");
        }
    }

    if ($oldversion < 2009050400) {
        $table = new XMLDBTable('artefact_pc_item2condition');
        $field = new XMLDBField('children');
        $field->setAttributes(XMLDB_TYPE_INTEGER, 1, null, XMLDB_NOTNULL, null, null, null, 0);
        add_field($table, $field);

        $table = new XMLDBTable('artefact_pc_condition');
        $field = new XMLDBField('core');
        $field->setAttributes(XMLDB_TYPE_INTEGER, 1, null, XMLDB_NOTNULL, null, null, null, 0);
        add_field($table, $field);
    }

    if ($oldversion < 2009081101) {
        $table = new XMLDBTable('artefact_pc_itemmap');
        $field = new XMLDBField('childiscore');
        $field->setAttributes(XMLDB_TYPE_INTEGER, 1, null, XMLDB_NOTNULL, null, null, null, 0);
        add_field($table, $field);

        $table = new XMLDBTable('artefact_pc_item');
        $field = new XMLDBField('core');
        drop_field($table, $field);
    }

    if ($oldversion < 2011101800) {
        // Replace pathsep char in item names
        $items = get_records_array('artefact_pc_item');
        foreach ($items as $i) {
            $name = str_replace(ARTEFACTPC_PATHSEP, ',', $i->item);
            set_field('artefact_pc_item', 'item', $name, 'id', $i->id);
        }

        // Make category nullable in item table
        $table = new XMLDBTable('artefact_pc_item');
        $field = new XMLDBField('category');
        $field->setAttributes(XMLDB_TYPE_CHAR, 255, XMLDB_UNSIGNED, null);
        change_field_notnull($table, $field);

        // Merge items with the same name, and remove the category, unless parent is ROOT
        $items = get_records_sql_array("
            SELECT * FROM {artefact_pc_item} WHERE id IN (
                SELECT childid FROM {artefact_pc_itemmap} WHERE NOT parentid IN (
                    SELECT id FROM {artefact_pc_item} WHERE item = 'ROOT'
                )
            )
            ORDER BY item, id",
            array()
        );

        $lastitem = $lastid = null;
        $replace = array();
        foreach ($items as $i) {
            if ($i->item == $lastitem) {
                $replace[$i->id] = $lastid;
            }
            $lastid = $i->id;
            $lastitem = $i->item;
        }

        $oldidlist = join(',', array_keys($replace));
        if ($oldidlist) {
            $casenotes = get_records_select_array(
                'artefact_pc_item2casenote',
                'itemid IN (' .  $oldidlist . ')',
                array()
            );
            if ($casenotes) {
                foreach ($casenotes as $c) {
                    $newid = $replace[$c->itemid];
                    if (!record_exists('artefact_pc_item2casenote', 'itemid', $newid, 'casenoteid', $c->casenoteid)) {
                        insert_record('artefact_pc_item2casenote', (object) array('itemid' => $newid, 'casenoteid' => $c->casenoteid));
                    }
                    delete_records('artefact_pc_item2casenote', 'itemid', $c->itemid, 'casenoteid', $c->casenoteid);
                }
            }
            $rels = get_records_select_array(
                'artefact_pc_itemmap',
                'parentid IN (' . $oldidlist . ') OR childid IN (' . $oldidlist . ')',
                array()
            );
            if ($rels) {
                foreach ($rels as $r) {
                    $newparent = isset($replace[$r->parentid]) ? $replace[$r->parentid] : $r->parentid;
                    $newchild = isset($replace[$r->childid]) ? $replace[$r->childid] : $r->childid;
                    if (!record_exists('artefact_pc_itemmap', 'parentid', $newparent, 'childid', $newchild)) {
                        insert_record(
                            'artefact_pc_itemmap',
                            (object) array(
                                'parentid' => $newparent,
                                'childid' => $newchild,
                                'childiscore' => $r->childiscore,
                            )
                        );
                    }
                    delete_records('artefact_pc_itemmap', 'parentid', $r->parentid, 'childid', $r->childid);
                }
            }
            delete_records_select('artefact_pc_item', 'id IN (' . $oldidlist . ')');
        }

        $sql = 'UPDATE {artefact_pc_item} SET category = NULL';

        $rootids = get_column('artefact_pc_item', 'id', 'item', 'ROOT');

        if ($rootids) {
            $sql .= '
            WHERE id IN (
                SELECT childid FROM {artefact_pc_itemmap} WHERE NOT parentid IN (' . join (',', $rootids) . ')
            )';
        }

        execute_sql($sql);
    }

    return true;
}
