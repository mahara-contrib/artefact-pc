<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-casenote
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

class PluginBlocktypeCasenote extends PluginBlocktype {

    public static function get_title() {
        return get_string('title', 'blocktype.pc/casenote');
    }

    /**
     * Optional method. If exists, allows this class to decide the title for
     * all blockinstances of this type
     */
    public static function get_instance_title(BlockInstance $bi) {
        $configdata = $bi->get('configdata');

        if (!empty($configdata['artefactid'])) {
            require_once(get_config('docroot') . 'artefact/lib.php');
            $casenote = artefact_instance_from_id($configdata['artefactid']);
            return $casenote->get('title');
        }
        return '';
    }

    public static function get_description() {
        return get_string('description', 'blocktype.pc/casenote');
    }

    public static function get_categories() {
        return array('general');
    }

    public static function render_instance(BlockInstance $instance, $editing=false) {
        $configdata = $instance->get('configdata');

        $result = '';
        if (!empty($configdata['artefactid'])) {
            require_once(get_config('docroot') . 'artefact/lib.php');
            $casenote = $instance->get_artefact_instance($configdata['artefactid']);
            $configdata['hidetitle'] = true;
            $configdata['viewid'] = $instance->get('view');
            $result = $casenote->render_self($configdata);
            $result = $result['html'];
        }

        return $result;
    }

    /**
     * Returns a list of artefact IDs that are in this blockinstance.
     *
     * Normally this would just include the casenote ID itself (children such
     * as attachments don't need to be included here, they're handled by the
     * artefact parent cache). But people might just link to artefacts without
     * using the attachment facility. There's nothing wrong with them doing
     * that, so if they do we should scrape the post looking for such links and
     * include those artefacts as being part of this blockinstance.
     *
     * @return array List of artefact IDs that are 'in' this casenote - all
     *               the casenote ID plus links to other artefacts that are
     *               part of the casenote text. Note that proper artefact
     *               children, such as casenote attachments, aren't included -
     *               the artefact parent cache is used for them
     * @see PluginBlocktypePc::get_artefacts()
     */
    public static function get_artefacts(BlockInstance $instance) {
        $configdata = $instance->get('configdata');
        $artefacts = array();
        if (isset($configdata['artefactid'])) {
            $artefacts[] = $configdata['artefactid'];

            // Add all artefacts found in the casenote text
            $casenote = $instance->get_artefact_instance($configdata['artefactid']);
            $artefacts = array_unique(array_merge($artefacts, $casenote->get_referenced_artefacts_from_casenote()));
        }
        return $artefacts;
    }

    public static function has_instance_config() {
        return true;
    }

    public static function instance_config_form($instance) {
        global $USER;
        safe_require('artefact', 'pc');
        $configdata = $instance->get('configdata');

        if (!empty($configdata['artefactid'])) {
            $casenote = $instance->get_artefact_instance($configdata['artefactid']);
        }

        $elements = array();

        // If the case note in this block is owned by the owner of the View,
        // then the block can be configured. Otherwise, the case note was
        // choose a case note to put in this block, when the one that is
        // copied in from another View. We won't confuse users by asking them to
        // currently in it isn't choosable.
        //
        // Note: the owner check will have to change when we do group/site
        // casenotes
        if (empty($configdata['artefactid']) || $casenote->get('owner') == $USER->get('id')) {
            $elements[] = self::artefactchooser_element((isset($configdata['artefactid'])) ? $configdata['artefactid'] : null);
            $elements[] = PluginArtefactPc::block_advanced_options_element($configdata, 'casenote');
        }
        else {
            $elements[] = array(
                'type' => 'html',
                'name' => 'notice',
                'value' => '<div class="message">' . get_string('casenotecopiedfromanotherview', 'artefact.pc', get_string('casenote', 'artefact.pc')) . '</div>',
            );
        }
        return $elements;
    }

    public static function artefactchooser_element($default=null) {
        $extrajoin   = ' JOIN {artefact_pc_casenote} ON {artefact_pc_casenote}.id = a.id ';

        $element = array(
            'name'  => 'artefactid',
            'type'  => 'artefactchooser',
            'title' => get_string('casenote', 'artefact.pc'),
            'defaultvalue' => $default,
            'blocktype' => 'casenote',
            'limit'     => 10,
            'selectone' => true,
            'artefacttypes' => array('casenote'),
            'extrajoin' => $extrajoin,
            //'extracols' => '1 - {artefact_pc_casenote}.published AS draft',
            'template'  => 'artefact:pc:artefactchooser-element.tpl',
        );
        return $element;
    }

    /**
     * Optional method. If specified, changes the order in which the artefacts are sorted in the artefact chooser.
     */
    public static function artefactchooser_get_sort_order() {
        return array(
            array('fieldname' => 'parent'),
            array('fieldname' => 'ctime', 'order' => 'DESC')
        );
    }

    public static function default_copy_type() {
        return 'nocopy';
    }

    /**
     * Casenote blocktype is only allowed in personal views, because currently
     * there's no such thing as group/site casenotes
     */
    public static function allowed_in_view(View $view) {
        return $view->get('owner') != null;
    }

}
