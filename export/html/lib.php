<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-pc-export-html
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

/**
 * Dumps all of the user's casenotes as static HTML
 */
class HtmlExportPc extends HtmlExportArtefactPlugin {

    public function dump_export_data() {
        foreach ($this->exporter->get('artefacts') as $artefact) {
            if ($artefact->get('artefacttype') == 'casenote') {
                // Create directory for storing the casenote
                $dirname = PluginExportHtml::text_to_path($artefact->get('title'));
                if (!check_dir_exists($this->fileroot . $dirname)) {
                    throw new SystemException("Couldn't create casenote directory {$this->fileroot}{$dirname}");
                }

                // Render the first page of the blog
                $smarty = $this->exporter->get_smarty('../../../', 'casenote');
                $smarty->assign('page_heading', $artefact->get('title'));
                $smarty->assign('breadcrumbs', array(
                    array('text' => get_string('casenotes', 'artefact.pc')),
                    array('text' => $artefact->get('title'), 'path' => 'index.html'),
                ));
                $rendered = $artefact->render_self(array('hidetitle' => true));
                $outputfilter = new HtmlExportPcOutputFilter('../../../', $this->exporter);
                $smarty->assign('rendered_casenote', $outputfilter->filter($rendered['html']));
                $content = $smarty->fetch('export:html/pc:index.tpl');

                if (false === file_put_contents($this->fileroot . $dirname . '/index.html', $content)) {
                    throw new SystemException("Unable to create index.html for casenote $artefact->get('id')");
                }
            }
        }
    }

    public function get_summary() {
        $smarty = $this->exporter->get_smarty();
        $casenotes = array();
        foreach ($this->exporter->get('artefacts') as $artefact) {
            if ($artefact->get('artefacttype') == 'casenote') {
                $casenotes[] = array(
                    'link' => 'files/pc/' . PluginExportHtml::text_to_path($artefact->get('title')) . '/index.html',
                    'title' => $artefact->get('title'),
                );
            }
        }
        if ($casenotes) {
            $smarty->assign('casenotes', $casenotes);

            $stryouhavecasenotes = (count($casenotes) == 1)
                ? get_string('youhaveonecasenote', 'artefact.pc')
                : get_string('youhavecasenotes', 'artefact.pc', count($casenotes));
        }
        else {
            $stryouhavecasenotes = get_string('youhavenocasenotes', 'artefact.pc');
        }

        $smarty->assign('stryouhavecasenotes', $stryouhavecasenotes);
        return array(
            'title' => get_string('casenotes', 'artefact.pc'),
            'description' => $smarty->fetch('export:html/pc:summary.tpl'),
        );
    }

    public function get_summary_weight() {
        return 40;
    }

}

class HtmlExportPcOutputFilter extends HtmlExportOutputFilter {

    /**
     * Additional HTML Ouput Filters for the problems and conditions artefact
     * Always call parent class after
     *
     * @param string $html The HTML to filter
     * @return string      The filtered HTML
     */
    public function filter($html) {
        // Links to conditions
        $matchstr = "/<a href.*?artefact\/pc\/.*?>(.*?)<\/a>/"; 
        $html = preg_replace($matchstr, "$1", $html);

        // Shared info
        $matchstr = "/<div class=\"shared\">.*?<\/div>/s";
        $html = preg_replace($matchstr, "", $html);

        return parent::filter($html);
    }

}
