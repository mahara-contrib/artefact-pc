<p>{$stryouhavecasenotes}</p>
{if $casenotes}
<ul>
{foreach from=$casenotes item=casenote}
    <li><a href="{$casenote.link}">{$casenote.title}</a></li>
{/foreach}
</ul>
{/if}
