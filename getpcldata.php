<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-pc
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */
exit;
// This file refers to nonexistent tables and doesn't appear to be used anywhere.

define('INTERNAL', true);
define('MENUITEM', 'myportfolio/pc');
define('SECTION_PLUGINTYPE', 'artefact');
define('SECTION_PLUGINNAME', 'pc');
require(dirname(dirname(dirname(__FILE__))) . '/init.php');
require_once('pieforms/pieform.php');

safe_require('artefact', 'pc');
safe_require('artefact', 'file');

($db_items= get_records_sql_array("SELECT * FROM {apc_item} ORDER BY item", array()))
|| ($db_items = array());
($db_conditions = get_records_sql_array("SELECT DISTINCT condition, core FROM {apc_condition} ORDER BY condition", array()))
|| ($db_conditions = array());
//log_debug($conditions);

echo "<html><head><title>Get PCL data</title></head><body>";
echo "<p>Generating XML</p>";
echo "<table><tbody>";
/*
foreach ($db_conditions as $db_condition) {
    echo '<tr><td>' . $db_condition->condition . '</td><td>' . $db_condition->core . '</td></tr>';
}
*/

$pac = new DOMDocument('1.0');
$pac->formatOutput = true;

// Create root element
$root = $pac->createElement('problemsandconditions');
$root = $pac->appendChild($root);

// Create major category notes
$categories = $pac->createElement('categories');
$categories = $root->appendChild($categories);
$items = $pac->createElement('items');
$items = $root->appendChild($items);
$conditions = $pac->createElement('conditions');
$conditions = $root->appendChild($conditions);

$category = $pac->createElement('category');
$category = $categories->appendChild($category);
$name = $pac->createElement('name');
$name = $category->appendChild($name);
$text = $pac->createTextNode('atoz');
$text = $name->appendChild($text);

// Add items
foreach ($db_items as $db_item) {
    $item = $pac->createElement('item');
    $item = $items->appendChild($item);

    $name = $pac->createElement('name');
    $name = $item->appendChild($name);
    $text = $pac->createTextNode($db_item->item);
    $text = $name->appendChild($text);

    $category = $pac->createElement('category');
    $category = $item->appendChild($category);
    $text = $pac->createTextNode('atoz');
    $text = $category->appendChild($text);
}

$processed = array();
$multiples = array();
foreach ($db_conditions as $db_condition) {
    if (!in_array($db_condition->condition, $processed)) {
        $processed[] = $db_condition->condition;
    }
    else {
        echo '<tr><td>' . $db_condition->condition . '</td><td>' . $db_condition->core . '</td></tr>';
        if (!in_array($db_condition->condition, $multiples)) {
            $multiples[] = $db_condition->condition;
        }
    }
}
foreach ($multiples as $multiple) {
    delete_records_sql("DELETE FROM {apc_condition} WHERE condition = ? AND core = 0", array($multiple));
}

// FIXME: sometimes end up with <rels/>, why?
// FIXME: the data contains some hairy things that get turned into stuff like &amp;lt;
foreach ($db_conditions as $db_condition) {
    //echo '<tr><td>' . $db_condition->condition . '</td><td>' . $db_condition->core . '</td></tr>';
    $condition = $pac->createElement('condition');
    $condition = $conditions->appendChild($condition);

    // Add name
    $name = $pac->createElement('name');
    $name = $condition->appendChild($name);
    $text = $pac->createTextNode($db_condition->condition);
    $text = $name->appendChild($text);

    // Add core
    $core = $pac->createElement('core');
    $core = $condition->appendChild($core);
    $text = $pac->createTextNode($db_condition->core);
    $text = $core->appendChild($text);

    // No need for parent with only atoz data
    //($db_item2condition = get_records_sql_array("SELECT item, parent FROM {apc_item2condition_atoz} ORDER BY condition", array()))
    ($db_item2condition = get_records_sql_array("SELECT DISTINCT item FROM {apc_item2condition_atoz} WHERE condition = ? ORDER BY item", array($db_condition->condition)))
    || ($db_item2condition = array());

    // Add rels
    $rels = $pac->createElement('rels');
    $rels = $condition->appendChild($rels);
    foreach ($db_item2condition as $i2c) {
        $rel = $pac->createElement('rel');
        $rel = $rels->appendChild($rel);

        $item = $pac->createElement('item');
        $item = $rel->appendChild($item);
        $text = $pac->createTextNode($i2c->item);
        $text = $item->appendChild($text);
    }
}

echo "</tbody></table>";
echo "<p>Done</p>";
echo "</body></html>";

//log_debug("Outputting the document:");
//log_debug($pac->saveXML());

log_debug("Saving the document.");
log_debug('Wrote: ' . $pac->save("/tmp/pcldata.xml") . ' bytes');

/*
foreach ($pac->childNodes as $item) {
    log_debug($item->nodeName . " = " . $item->nodeValue);
    foreach ($item->childNodes as $item) {
        log_debug($item->nodeName . " = " . $item->nodeValue);
    }
}
*/

?>
