<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-pc
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

define('INTERNAL', 1);
define('JSON', 1);
require(dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/init.php');
require('conditionsbrowser.php');
safe_require('artefact', 'pc');


$cb = new StdClass;

$cb->filter = array();
$cb->filter['column']   = param_variable('column', -1);
$cb->filter['type']     = param_variable('filtertype');
$cb->filter['core']     = param_integer('core', 0);
$cb->filter['shared']   = param_integer('shared', 0);
$cb->filter['sort']     = param_variable('sort', 'dateadded');
$cb->filter['order']    = param_alpha('order', 'D');
$cb->filter['children'] = param_integer('children', 0);
$cb->filter['selected'] = param_variable('selected', '');
$cb->filter['path']     = param_variable('path', '');
$cb->filter['values']   = (!empty($cb->filter['path'])) ? explode(ARTEFACTPC_PATHSEP, $cb->filter['path']) : array();

get_data($cb);

$smarty = smarty_core();
$smarty->assign('filtertype', $cb->filter['type']);
$smarty->assign('core', $cb->filter['core']);
if ($cb->filter['column'] < 0) {
    $smarty->assign('data', $cb->data);
    $template = 'conditionsbrowser.tpl';
}
else {
    $smarty->assign('count', $cb->filter['column'] + 1);
    $smarty->assign('columndata', $cb->data[$cb->filter['column'] + 1]);
    $template = 'conditionsbrowsercolumn.tpl';
}

$html = $smarty->fetch('artefact:pc:form/' . $template);

json_reply(false, array(
    'message' => null,
    'html' => $html,
));
