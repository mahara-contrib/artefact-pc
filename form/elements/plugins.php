<?php

class PluginPc {
    protected static $plugins = array();

    public function __construct() {
        $plugindir = scandir(dirname(__FILE__) . '/plugins/');
        foreach ($plugindir as $file) {
            if (substr($file, -4) == '.php') {
                require_once 'plugins/' . $file;
                $classname = 'PluginPc' . strtoupper(substr($file, 0, 1)) . substr($file, 1, -4);
                if (class_exists($classname)) {
                    self::$plugins[] = $classname;
                }
            }
        }
    }

    public function get_plugins() {
        return self::$plugins;
    }

    public function get_data(&$data) {
        if (isset($data->filter['column'])) {
            if ($data->filter['column'] == -1) {
                if (!empty($data->filter['path'])) {
                    $data->filter['values'] = explode(ARTEFACTPC_PATHSEP, $data->filter['path']);
                    $data->filter['path'] = '';
                    for ($i = 0; $i < count($data->filter['values']); $i++) {
                        $data->filter['path'] .= ($i == 0) ? $data->filter['values'][$i] : ARTEFACTPC_PATHSEP . $data->filter['values'][$i];
                        self::get_items($data, $i);
                    }

                    foreach ($data->data[count($data->data) - 1]->items as $key => $value) {
                        if (isset($data->data[count($data->data) - 1]->selected) && ($value->itemkey == $data->data[count($data->data) - 1]->selected)) {
                            self::get_items($data, count($data->data));
                        }
                    }
                }
                else {
                    self::get_items($data);
                }
            }
            else {
                if (!empty($data->filter['children'])) {
                    self::get_items($data, $data->filter['column']);
                    self::get_items($data, $data->filter['column'] + 1);
                }
                else {
                    self::get_items($data, $data->filter['column']);
                }
            }
        }
        else {
            if (empty($data->filter['query'])) {
                self::get_items($data);
            }
            else {
                if ($data->filter['type'] == 'atoz' && !empty($data->filter['magic'])) {
                    // When magic abides...
                    $sql_itemid = "SELECT id, item FROM {artefact_pc_item} WHERE (category = ? OR category IS NULL) AND item = ?";
                    $sqlvalues_itemid = array($data->filter['type'], $data->filter['query']);
                    $item = get_records_sql_array($sql_itemid, $sqlvalues_itemid);
                    $ids = array();

                    $query = '';

                    while (!empty($item[0]->id) && $item[0]->item != 'ROOT') {
                        $query = !empty($query) ? $item[0]->item . ARTEFACTPC_PATHSEP . $query : $item[0]->item;
                        array_unshift($ids, $item[0]->id);

                        $sql = "SELECT im.parentid AS id, i.item FROM {artefact_pc_itemmap} im JOIN {artefact_pc_item} i ON (im.parentid = i.id) WHERE childid = ? LIMIT 1";
                        $sqlvalues = array($item[0]->id);

                        unset($item);
                        $item = get_records_sql_array($sql, $sqlvalues);
                    }

                    $data->filter['query'] = $query;
                }

                $data->filter['values'] = explode(ARTEFACTPC_PATHSEP, $data->filter['query']);
                $data->filter['path'] = '';
                for ($i = 0; $i < count($data->filter['values']); $i++) {
                    $data->filter['path'] .= ($i == 0) ? $data->filter['values'][$i] : ARTEFACTPC_PATHSEP . $data->filter['values'][$i];
                    self::get_items($data, $i);
                }
                foreach ($data->data[count($data->data) - 1]->items as $key => $value) {
                    if (isset($data->data[count($data->data) - 1]->selected) && ($value->itemkey == $data->data[count($data->data) - 1]->selected)) {
                        self::get_items($data, count($data->data));
                    }
                }
            }
        }
    }

    public function get_items(&$data, $column = 0) {
        if ($column < 0) {
            throw new SystemException(get_string('invalidcolumn', 'artefact.pc', $column));
        }
        $results = new StdClass;

        $core = (!empty($data->filter['core'])) ? true : false;
        $shared = (!empty($data->filter['shared'])) ? true : false;
        $parentitem = (!empty($data->filter['parent'])) ? $data->filter['parent'] : '';
        $values = (!empty($data->filter['values'])) ? true : false;

        $query = array();
        $sqlvalues = array();

        if ($column == 0) {
            if ($values) {
                $results->selected = $data->filter['values'][0];
            }
        }
        else {
            if ($values) {
                if (isset($data->filter['values'][$column - 1])) {
                    $value = $data->filter['values'][$column - 1];
                }
                else {
                    throw new SystemException('You have no parent value to filter on');
                }
                if (isset($data->filter['values'][$column])) {
                    $results->selected = $data->filter['values'][$column];
                }
            }
            else {
                throw new SystemException('You have no values');
            }
            // Set parentitem
            $results->parentitem = $value;
        }
        $results->column = $column;
        $results->filtertype = $data->filter['type'];

        $sql = "
        SELECT
            c.item AS itemkey, c.id, COUNT(cm.parentid) AS children
        FROM
            {artefact_pc_item} p
            JOIN {artefact_pc_itemmap} im ON p.id = im.parentid
            JOIN {artefact_pc_item} c ON im.childid = c.id
            LEFT JOIN {artefact_pc_itemmap} cm ON cm.parentid = c.id" . ($core ? ' AND cm.childiscore = 1' : '') . "
        WHERE
            p.item = ?
            AND (p.category = ? OR p.category IS NULL)" . ($core ? ' AND im.childiscore = 1' : '') . "
        GROUP BY c.item, c.id
        ORDER BY c.item";

        $sqlvalues = array(
            $column == 0 ? 'ROOT' : $value,
            $data->filter['type'],
        );

        ($items = get_records_sql_array($sql, $sqlvalues))
        || ($items = array());

        $results->items = $items;

        // Grab some extra data for each item, including the number of case
        // notes tagged with it
        // TODO: optimisation: pass correct $value (4th parameter) here
        $itemgraph = ArtefactTypeCasenote::get_itemgraph($data->filter['type'], $core, $shared);

        // Get language string values for items and item counts
        foreach ($results->items as $key => $value) {
            $name = get_string($results->items[$key]->itemkey, 'artefact.pc');
            $results->items[$key]->name = $name;
            if (isset($itemgraph[$results->items[$key]->id]->casenotes)) {
                $results->items[$key]->count = count($itemgraph[$results->items[$key]->id]->casenotes);
            }

            if ($column == 0) {
                $results->items[$key]->path = $results->items[$key]->itemkey;
            }
            else {
                $currentpath = explode(ARTEFACTPC_PATHSEP, $data->filter['path']);
                $currentpath[$column] = $results->items[$key]->itemkey;
                $results->items[$key]->path = implode(ARTEFACTPC_PATHSEP, $currentpath);
            }
        }

        $data->data[$column] = $results;
    }
}
