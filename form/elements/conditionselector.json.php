<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-pc
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

define('INTERNAL', 1);
define('JSON', 1);
require(dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/init.php');
require('conditionselector.php');
safe_require('artefact', 'pc');


$cs = new StdClass;

$cs->filter = array();
$cs->filter['column']   = param_integer('column', -1);
$cs->filter['type']     = param_variable('filtertype');
$cs->filter['core']     = param_integer('core', 0);
$cs->filter['shared']   = param_integer('shared', 0);
$cs->filter['sort']     = param_variable('sort', 'dateadded');
$cs->filter['order']    = param_alpha('order', 'D');
$cs->filter['children'] = param_integer('children', 0);
$cs->filter['path']     = param_variable('path', '');
$cs->filter['values']   = (!empty($cs->filter['path'])) ? explode(ARTEFACTPC_PATHSEP, $cs->filter['path']) : array();

get_data($cs);

$smarty = smarty_core();
$smarty->assign('filtertype', $cs->filter['type']);
$smarty->assign('core', $cs->filter['core']);
if ($cs->filter['column'] < 0) {
    $smarty->assign('data', $cs->data);
    $template = 'conditionselector.tpl';
}
else {
    $smarty->assign('count', $cs->filter['column'] + 1);
    $smarty->assign('columndata', $cs->data[$cs->filter['column'] + 1]);
    $template = 'conditionselectorcolumn.tpl';
}

$html = $smarty->fetch('artefact:pc:form/' . $template);

json_reply(false, array(
    'message' => null,
    'html' => $html,
));
