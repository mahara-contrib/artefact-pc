<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-pc
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

require_once('plugins.php');
if (empty($GLOBALS['pluginpc'])) $GLOBALS['pluginpc'] = new PluginPc();

function pieform_element_conditionselector(Pieform $form, $element) {
    global $USER;
    $smarty = smarty_core();
    $userid = $USER->get('id');

    $cs = new StdClass;
    $cs->filter = array(
        //'type'   => (!empty($element['filtertype'])) ? $element['filtertype'] : 'specialty', // Default to specialty filter
        'type'   => (!empty($element['filtertype'])) ? $element['filtertype'] : '',
        'sort'   => 'dateadded',
        'order'  => 'D',
        //'query'  => $element['query'],
    );
    $cs->data = array();
    get_categories($cs);
    get_data($cs);

    $smarty->assign('categories', $cs->categories);
    $smarty->assign('filtertype', $cs->filter['type']);
    $smarty->assign('data', $cs->data);

    $id = param_integer('id', 0);
    ($selectedlist = get_records_sql_array("
        SELECT item AS itemkey, id
        FROM {artefact_pc_item}
        WHERE id IN (
            SELECT itemid
            FROM {artefact_pc_item2casenote}
            WHERE casenoteid = ?
        )
        ORDER BY item",
        array($id)
    ))
    || ($selectedlist = array());

    foreach ($selectedlist as $condition) {
        $name = get_string($condition->itemkey, 'artefact.pc');
        if (substr($name, 0, 2) == '[[') {
            $condition->name = $condition->itemkey;
        }
        else {
            $condition->name = $name;
        }
    }

    $smarty->assign('selectedlist', $selectedlist);

    $prefix = $form->get_name() . '_' . $element['name'];
    $pathsep = json_encode(ARTEFACTPC_PATHSEP);
    $initjs = "var {$prefix} = new ConditionSelector('{$prefix}', {$pathsep});";
    $initjs .= "addLoadEvent({$prefix}.init);";

    $smarty->assign('prefix', $prefix);
    $smarty->assign('initjs', $initjs);

    return $smarty->fetch('artefact:pc:form/conditionselectorouter.tpl');
}

function get_categories(&$cs) {
    ($items = get_column_sql("SELECT category FROM {artefact_pc_category} ORDER BY category", null))
    || ($items = array());

    $cs->categories = new StdClass;
    if (empty($cs->filter['type']) && !empty($items)) {
        $atoz = array_search('atoz', $items);
        $cs->filter['type'] = (!empty($items) && $atoz) ? $items[$atoz] : $items[0];
    }
    $categories = array();
    foreach ($items as $item) {
      $categories[$item] = get_string($item, 'artefact.pc');
    }
    $cs->categories->items = $categories;
}

function get_data(&$cs) {
    $classname = 'PluginPc' . $cs->filter['type'];
    // Uppercase the character after 'PluginPc'
    $classname[8] = strtoupper($classname[8]);

    $plugins = PluginPc::get_plugins();
    if (in_array($classname, $plugins)) {
        call_user_func(array($classname, 'get_data'), $cs);
    }
    else {
        $cs->data[0] = new StdClass;
        $cs->data[0]->column = 0;
        $cs->data[0]->filtertype = $cs->filter['type'];
        $cs->data[0]->items = array();
    }
}

function pieform_element_conditionselector_get_value(Pieform $form, $element) {
    // Check if the user tried to make a change to the conditionselector element
    if ($form->is_submitted()) {
        $prefix = $form->get_name() . '_' . $element['name'];
        $selected = param_variable($prefix . '_selected', null);
        $result = array();

        if (is_array($selected)) {
            // When files are being selected, this element has a real value
            $result['selected'] = array_keys($selected);
        }

        return $result;
    }
}

function pieform_element_conditionselector_get_headdata($element) {
    $headdata = array(
        '<script type="text/javascript" src="' . get_config('wwwroot') . 'artefact/pc/js/conditionselector.js"></script>',
        '<script type="text/javascript" src="' . get_config('wwwroot') . 'artefact/pc/js/grippies.js"></script>',
    );
    return $headdata;
}
