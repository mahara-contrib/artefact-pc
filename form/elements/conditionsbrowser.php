<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-pc
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

require_once('plugins.php');
if (empty($GLOBALS['pluginpc'])) $GLOBALS['pluginpc'] = new PluginPc();

function pieform_element_conditionsbrowser(Pieform $form, $element) {
    global $USER;
    $smarty = smarty_core();
    $userid = $USER->get('id');

    $cb = new StdClass;
    $cb->filter = array(
        //'type'   => (!empty($element['filtertype'])) ? $element['filtertype'] : 'specialty', // Default to specialty filter
        'type'   => (!empty($element['filtertype'])) ? $element['filtertype'] : '',
        'sort'   => 'dateadded',
        'order'  => 'D',
        'query'  => $element['query'],
        'magic'  => (!empty($element['magic'])) ? true : false,
    );
    $cb->data = array();
    get_categories($cb);
    get_data($cb);

    $smarty->assign('categories', $cb->categories);
    $smarty->assign('filtertype', $cb->filter['type']);
    $smarty->assign('path', $element['query']);
    $smarty->assign('data', $cb->data);

    $prefix = $form->get_name() . '_' . $element['name'];
    $pathsep = json_encode(ARTEFACTPC_PATHSEP);
    $initjs = "var {$prefix} = new ConditionsBrowser('{$prefix}', {$pathsep});";
    $initjs .= "addLoadEvent({$prefix}.init);";
    $smarty->assign('initjs', $initjs);

    return $smarty->fetch('artefact:pc:form/conditionsbrowserouter.tpl');
}

function get_categories(&$cb) {
    ($items = get_column_sql("SELECT category FROM {artefact_pc_category} ORDER BY category", null))
    || ($items = array());

    $cb->categories = new StdClass;
    if (empty($cb->filter['type']) && !empty($items)) {
        $atoz = array_search('atoz', $items);
        $cb->filter['type'] = (!empty($items) && $atoz) ? $items[$atoz] : $items[0];
    }
    $categories = array();
    foreach ($items as $item) {
      $categories[$item] = get_string($item, 'artefact.pc');
    }
    $cb->categories->items = $categories;
}

function get_data(&$cb) {
    $classname = 'PluginPc' . $cb->filter['type'];
    // Uppercase the character after 'PluginPc'
    $classname[8] = strtoupper($classname[8]);

    $plugins = PluginPc::get_plugins();
    if (in_array($classname, $plugins)) {
        call_user_func(array($classname, 'get_data'), $cb);
    }
    else {
        $cb->data[0] = new StdClass;
        $cb->data[0]->column = 0;
        $cb->data[0]->filtertype = $cb->filter['type'];
        $cb->data[0]->items = array();
    }
}

function pieform_element_conditionsbrowser_get_value(Pieform $form, $element) {
    // Check if the user tried to make a change to the conditionsbrowser element
    if ($form->is_submitted()) {
        $result = array();
        return $result;
    }
}

function pieform_element_conditionsbrowser_get_headdata($element) {
    $headdata = array(
        '<script type="text/javascript" src="' . get_config('wwwroot') . 'artefact/pc/js/conditionsbrowser.js"></script>',
        '<script type="text/javascript" src="' . get_config('wwwroot') . 'artefact/pc/js/grippies.js"></script>',
    );
    return $headdata;
}
