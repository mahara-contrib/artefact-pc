<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-pc
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

define('INTERNAL', true);
define('MENUITEM', 'content/pc');
define('SECTION_PLUGINTYPE', 'artefact');
define('SECTION_PLUGINNAME', 'pc');
require(dirname(dirname(dirname(__FILE__))) . '/init.php');
require_once('pieforms/pieform.php');

safe_require('artefact', 'pc');
safe_require('artefact', 'file');

$id = param_integer('id', 0);
if (!$id) {
    redirect('/artefact/pc/');
}

$casenote = new ArtefactTypeCasenote($id);
$casenote->check_permission();

$title            = $casenote->get('title');
$date_encountered = $casenote->get('etime');
$date_recorded    = $casenote->get('ctime');
$notes            = $casenote->get('description');
$shared           = $casenote->get('shared');
$tags             = $casenote->get('tags'); // is array
$conditionitems   = $casenote->get('conditionsfull'); // is array
$pagetitle        = hsc(get_string('editcasenote', 'artefact.pc'));
$focuselement     = 'title';
define('TITLE', $pagetitle);

// Only display for manual editing tags that DO NOT MATCH linked conditions. 
foreach ($conditionitems as $condition) {
    $i = 0;
    foreach ($tags as $tag) {
        if ($tag == $condition->item) {
            unset($tags[$i]);
            $tags = array_values($tags);
        }
        $i++;
    }
}

// Turn tags into a string
$tags = join(', ', $tags);

$folder    = param_integer('folder', ArtefactTypeCasenote::casenote_files_folder_id());
$browse    = (int) param_variable('browse', 0);
$highlight = null;
if ($file = param_integer('file', 0)) {
    $highlight = array($file);
}

$form = pieform(array(
    'name'              => 'editcasenote',
    'method'            => 'post',
    'action'            => '',
    'autofocus'         => $focuselement,
    'jsform'            => true,
    'newiframeonsubmit' => true,
    'jssuccesscallback' => 'editcasenote_success',
    'jserrorcallback'   => 'editcasenote_error',
    'plugintype'        => 'artefact',
    'pluginname'        => 'pc',
    'configdirs'        => array(
        get_config('libroot') . 'form/',
        get_config('docroot') . 'artefact/file/form/',
        get_config('docroot') . 'artefact/pc/form/'
    ),
    'elements' => array(
        'title' => array(
            'type'  => 'text',
            'title' => get_string('casetitle', 'artefact.pc'),
            'rules' => array(
                'required' => true
            ),
            'defaultvalue' => $title,
        ),
        'date_encountered' => array(
            'type'         => 'date',
            'title'        => 'Date Encountered',
            'description'  => '',
            'defaultvalue' => $date_encountered,
            'rules' => array(
                'required'    => true
            )
        ),
        'date_recorded' => array(
            'type'         => 'hidden',
            'value' => $date_recorded,
            'rules' => array(
                'required'    => true
            )
        ),
        'tags' => array(
            'type' => 'text',
            'title' => 'Tags',
            'defaultvalue' => $tags,
        ),
        'notes' => array(
            'type'  => 'wysiwyg',
            'rows'  => 20,
            'cols'  => 70,
            'title' => get_string('casenotes', 'artefact.pc'),
            'rules' => array(
                'required' => true
            ),
            'defaultvalue' => $notes,
        ),
        'filebrowser' => array(
            'type'      => 'filebrowser',
            'title'     => get_string('attachments', 'artefact.pc'),
            'folder'    => $folder,
            'highlight' => $highlight,
            'browse'    => $browse,
            'page'      => get_config('wwwroot') . 'artefact/pc/editcasenote.php?id=' . $id . '&browse=1',
            'config' => array(
                'upload'          => true,
                'uploadagreement' => get_config_plugin('artefact', 'file', 'uploadagreement'),
                'createfolder'    => false,
                'edit'            => false,
                'select'          => true,
            ),
            'selectlistcallback' => 'load_attachments',
            'selectcallback'     => 'add_attachment',
            'unselectcallback'   => 'delete_attachment',
        ),
        'conditionselector' => array(
            'type' => 'conditionselector',
            'title' => get_string('conditions', 'artefact.pc'),
        ),
        'shared' => array(
            'type'  => 'checkbox',
            'title' => get_string('sharecasenote', 'artefact.pc'),
            'defaultvalue' => $shared,
        ),
        'submitcasenote'   => array(
            'type'  => 'submitcancel',
            'value' => array(get_string('savecasenote', 'artefact.pc'), get_string('cancel')),
            'goto'  => get_config('wwwroot') . 'artefact/pc',
        ),
    )
));

/*
 * Javascript specific to this page.  Creates the list of files
 * attached to the casenote.
 */
$javascript = <<<EOF
function editcasenote_success(form, data) {
    editcasenote_filebrowser.callback(form, data);
};
function editcasenote_error(form, data) {
    editcasenote_conditionselector.init();
};
EOF;


$smarty = smarty(array('tablerenderer'));
$smarty->assign_by_ref('PAGEHEADING', $pagetitle);
$smarty->assign_by_ref('INLINEJAVASCRIPT', $javascript);
$smarty->assign_by_ref('casenoteform', $form);
$smarty->assign('pagetitle', $pagetitle);
$smarty->display('artefact:pc:editcasenote.tpl');

function editcasenote_submit(Pieform $form, array $values) {
    global $USER, $SESSION, $id;

    // save the casenote if the user clicked submit or has no js
    $submitted = !empty($values['submitcasenote']);
    if ($submitted || !$form->submitted_by_js()) {
        db_begin();
        $casenote = new ArtefactTypeCasenote($id);
        $casenote->set('title', $values['title']);
        $casenote->set('etime', $values['date_encountered']);
        $casenote->set('ctime', $values['date_recorded']);
        $casenote->set('description', $values['notes']);
        $casenote->set('owner', $USER->get('id'));
        $casenote->set('shared', (int) $values['shared']);
        $casenote->set('tags', preg_split("/\s*,\s*/", trim($values['tags'])));
        if (!empty($values['conditionselector']['selected'])) {
            $casenote->set('conditions', $values['conditionselector']['selected']);
        }
        $casenote->commit();

        // Attachments
        $old = $casenote->attachment_id_list();
        $new = is_array($values['filebrowser']) ? $values['filebrowser'] : array();
        if (!empty($new) || !empty($old)) {
            foreach ($old as $o) {
                if (!in_array($o, $new)) {
                    $casenote->detach($o);
                }
            }
            foreach ($new as $n) {
                if (!in_array($n, $old)) {
                    $casenote->attach($n);
                }
            }
            $filebrowser['selectedlist'] = $casenote->get_attachments(true);
        }
        db_commit();

        if ($submitted) {
            $result = array(
                'error'   => false,
                'message' => get_string('casenotesaved', 'artefact.pc'),
                'goto'    => get_config('wwwroot') . 'artefact/pc/',
            );
            if ($form->submitted_by_js()) {
                // Redirect back to the case notes page from within the iframe
                $SESSION->add_ok_msg($result['message']);
                $form->json_reply(PIEFORM_OK, $result, false);
            }
            $form->reply(PIEFORM_OK, $result);
        }
    }

    // Non-js filebrowser submission
    $result = array(
        'error' => false,
        'goto'  => get_config('wwwroot') . 'artefact/pc/editcasenote.php?id=' . $id,
    );
    if (isset($values['filebrowser']['browse'])) {
        $result['goto'] .= '&browse=1';
    }
    if (isset($values['filebrowser']['folder'])) {
        $result['goto'] .= '&folder=' . $values['filebrowser']['folder'];
    }
    if (isset($values['filebrowser']['highlight'])) {
        $result['goto'] .= '&file=' . $values['filebrowser']['highlight'];
    }
    $form->reply(empty($result['error']) ? PIEFORM_OK : PIEFORM_ERR, $result);
}

function load_attachments() {
    global $casenote;
    if ($casenote) {
        return $casenote->get_attachments(true);
    }
    return array();
}

function add_attachment($attachmentid) {
    global $casenote;
    if ($casenote) {
        $casenote->attach($attachmentid);
    }
}

function delete_attachment($attachmentid) {
    global $casenote;
    if ($casenote) {
        $casenote->detach($attachmentid);
    }
}
